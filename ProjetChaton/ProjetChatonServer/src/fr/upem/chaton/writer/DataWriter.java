package fr.upem.chaton.writer;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.ProcessStatus;
import fr.upem.chaton.protocole.Protocole;
import fr.upem.chaton.protocole.Protocole.Atom;
import fr.upem.chaton.writer.atomic.AtomicWriter;

public class DataWriter {

	private enum State {
		DONE, WAITING_OP, WAITING, ERROR
	};

	private State state = State.WAITING_OP;
	private ByteBuffer bb;
	private Charset charset;

	private DataChaton data;
	private List<AtomicWriter> atomicWriters;
	private List<Atom> config;
	private int alreadyDo = 0;

	public DataWriter(ByteBuffer bbout, Charset charset) {
		this.bb = Objects.requireNonNull(bbout);
		this.charset = Objects.requireNonNull(charset);
	}

	private void processOp() {
		try {
			var type = data.getType();
			config = Protocole.getConfig(type);
			var builder = new DataWriterBuilder();
			atomicWriters = builder.setBB(bb).setCharset(charset).setProtocol(config).build();

			//System.out.println("Type d'opération envoyé:" + type);
		} catch (IllegalArgumentException e) { // Not found Op Exception
			state = State.ERROR;
			throw e;
		}
		state = State.WAITING;
	}

	/** 
	 * Put the list of data in the DataChaton in the buffer if it's possible and return the ProcessStatus corresponding
	 * @param data
	 * @return ProcessStatus
	 */
	public ProcessStatus process(DataChaton data) {
		Objects.requireNonNull(data);
		this.data = data;

		if (state == State.DONE || state == State.ERROR) {
			throw new IllegalStateException();
		}

		if (state == State.WAITING_OP) {
			processOp();
		}
		
		if (atomicWriters.size() != data.size()) {
			throw new IllegalStateException(
					"atomicWriters size :" + atomicWriters.size() + " data size :" + data.size());
		}

		while (alreadyDo < data.size()) {

			var writer = atomicWriters.get(alreadyDo);
			var value = data.get(alreadyDo);
			var typeAtom = config.get(alreadyDo);
			var status = DataWriterParser.parseAndPut(writer, value, typeAtom);

			if (status != ProcessStatus.DONE)
				return status;

			alreadyDo++;
		}

		reset();
		return ProcessStatus.DONE;
	}

	private void reset() {
		alreadyDo = 0;
		state = State.WAITING_OP;
	}
}
