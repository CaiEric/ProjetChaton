package fr.upem.chaton.writer;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import fr.upem.chaton.protocole.Protocole.Atom;
import fr.upem.chaton.writer.atomic.AtomicWriter;
import fr.upem.chaton.writer.atomic.IntWriter;
import fr.upem.chaton.writer.atomic.LongWriter;
import fr.upem.chaton.writer.atomic.OpWriter;
import fr.upem.chaton.writer.atomic.StringWriter;

public class DataWriterBuilder {
	private List<Atom> config;
	private ByteBuffer bb;
	private Charset charset;

	/**
	 * set the request protocole config
	 * @param config
	 * @return DataWriterBuilder
	 */
	public DataWriterBuilder setProtocol(List<Atom> config) {
		Objects.requireNonNull(config);
		this.config = config;
		return this;
	}

	/**
	 * set the buffer 
	 * @param bb
	 * @return DataWriterBuilder
	 */
	public DataWriterBuilder setBB(ByteBuffer bb) {
		Objects.requireNonNull(bb);
		this.bb = bb;
		return this;
	}

	/**
	 * set the charset 
	 * @param charset
	 * @returnDataWriterBuilder
	 */
	
	public DataWriterBuilder setCharset(Charset charset) {
		Objects.requireNonNull(charset);
		this.charset = charset;
		return this;
	}

	/** Build a list of writers corresponding to the config request protocole
	 * 
	 * @return AtomicWriter
	 */
	public List<AtomicWriter> build() {
		List<AtomicWriter> toDo = new ArrayList<>();

		for (var type : config) {
			switch (type) {
			case OP:
				toDo.add(new OpWriter(bb));
				break;
			case INT:
				toDo.add(new IntWriter(bb));
				break;
			case LONG:
				toDo.add(new LongWriter(bb));
				break;
			case STRING:
				toDo.add(new StringWriter(bb, charset));
				break;
			default:
				throw new IllegalArgumentException();
			}
		}
		return toDo;
	}
}
