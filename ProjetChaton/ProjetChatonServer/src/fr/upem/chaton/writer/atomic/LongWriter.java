package fr.upem.chaton.writer.atomic;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.chaton.ProcessStatus;

public class LongWriter implements AtomicWriter {

	private ByteBuffer bb;
	
	/**
	 * Construct the LongWriter
	 * @param bb
	 */
	public LongWriter(ByteBuffer bb) {
		this.bb = Objects.requireNonNull(bb);
	}

	/**
	 * Put a Long in the buffer if it's possible and return the ProcessStatus corresponding
	 * @param data
	 * @return ProcessStatus
	 */
	
	public ProcessStatus put(long data) {
		Objects.requireNonNull(data);

		if (bb.capacity() < Long.BYTES) {
			throw new IllegalArgumentException();
		}

		if (bb.remaining() < Long.BYTES) {
			return ProcessStatus.REFILL;
		}

		bb.putLong(data);
		return ProcessStatus.DONE;
	}
}
