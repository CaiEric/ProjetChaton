package fr.upem.chaton.protocole;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Protocole {
	public enum Atom {
		OP, INT, LONG, STRING, HTTP
	};

	private static final HashMap<Integer, TypeRequest> opMap = new HashMap<>();
	private static final HashMap<TypeRequest, List<Atom>> typeRequestMap = new HashMap<>();

	static {
		mapping(0, TypeRequest.AUTHENTIFICATION, Arrays.asList(Atom.OP, Atom.STRING));
		mapping(1, TypeRequest.PRIVATE_MESSAGE, Arrays.asList(Atom.OP, Atom.STRING, Atom.STRING));
		mapping(2, TypeRequest.PUBLIC_MESSAGE, Arrays.asList(Atom.OP, Atom.STRING, Atom.STRING));
		mapping(3, TypeRequest.PRIVATE_CONNECTION, Arrays.asList(Atom.OP, Atom.STRING));
		mapping(4, TypeRequest.ACCEPT, Arrays.asList(Atom.OP, Atom.STRING));
		mapping(5, TypeRequest.REFUSE, Arrays.asList(Atom.OP, Atom.STRING));
		mapping(6, TypeRequest.TOKEN_SERVER, Arrays.asList(Atom.OP, Atom.LONG, Atom.STRING));
		mapping(7, TypeRequest.TOKEN_CLIENT, Arrays.asList(Atom.OP, Atom.LONG));
		typeRequestMap.put(TypeRequest.HTTP_GET, Arrays.asList(Atom.HTTP));
	}

	/**
	 * Create 2 key, value --> (OpCode, TypeRequest) and (TypeRequest, List defining the request protocole)
	 * @param opCode -> Integer
	 * @param type -> TypeRequest
	 * @param list -> List<Atom>
	 */
	private static void mapping(int opCode, TypeRequest type, List<Atom> list) {
		opMap.put(opCode, type);
		typeRequestMap.put(type, list);
	}

	/**
	 * Get the request protocole config with the Opcode
	 * @return the config
	 * @throws IllegalArgumentException when not op found in the map
	 */
	public static List<Atom> getConfig(int op) {
		if (opMap.containsKey(op)) {
			return Collections.unmodifiableList(typeRequestMap.get(opMap.get(op)));
		}
		throw new IllegalArgumentException("The op not found in map");
	}

	/**
	 * Get the request protocole config with the type
	 * @return the config
	 * @throws IllegalArgumentException when not op found in the map
	 */
	public static List<Atom> getConfig(TypeRequest type) {
		Objects.requireNonNull(type);
		if (typeRequestMap.containsKey(type)) {
			return Collections.unmodifiableList(typeRequestMap.get(type));
		}
		throw new IllegalArgumentException("The op not found in map");
	}

	/**
	 * Get the type with the opCode
	 * @throws IllegalArgumentException
	 */
	public static TypeRequest getTypeRequest(int op) {
		if (opMap.containsKey(op)) {
			return opMap.get(op);
		}
		throw new IllegalArgumentException();
	}
}
