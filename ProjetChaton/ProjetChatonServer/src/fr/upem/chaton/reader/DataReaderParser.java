package fr.upem.chaton.reader;

import java.util.Objects;

import fr.upem.chaton.protocole.Protocole.Atom;
import fr.upem.chaton.reader.atomic.AtomicReader;

public interface DataReaderParser {

	/**
	 * Parse the value 
	 * @param atomicReader
	 * @param type
	 * @return
	 */
	public static String parse(AtomicReader atomicReader, Atom type) {
		Objects.requireNonNull(atomicReader);
		Objects.requireNonNull(type);
		switch (type) {
		case OP:
			return String.valueOf((byte) atomicReader.get());
		case INT:
			return String.valueOf((int) atomicReader.get());
		case LONG:
			return String.valueOf((long) atomicReader.get());
		case STRING:
			return (String) atomicReader.get();
		default:
			throw new IllegalArgumentException("Type of AtomicReader is not available");
		}
	}
}
