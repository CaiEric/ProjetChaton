package fr.upem.chaton.reader.atomic;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Objects;

import fr.upem.chaton.ProcessStatus;

public class StringReader implements AtomicReader {

	private enum State {
		DONE, WAITING_INT, WAITING, ERROR
	};

	private State state = State.WAITING_INT;
	private ByteBuffer bb;
	private IntReader intReader;
	private Charset charset;
	private int size;
	private String string;

	public StringReader(ByteBuffer bb, Charset charset) {
		this.bb = Objects.requireNonNull(bb);
		this.charset = Objects.requireNonNull(charset);
		this.intReader = new IntReader(bb);
	}

	/**
	 * Read a String if it's possibleand return the ProcessStatus corresponding
	 * @return ProcessStatus
	 */
	@Override
	public ProcessStatus process() {
		if (state == State.DONE || state == State.ERROR) {
			throw new IllegalStateException();
		}

		if (state == State.WAITING_INT) {
			var status = intReader.process();
			if (status != ProcessStatus.DONE) {
				return status;
			}

			size = (int) intReader.get();

			if (size < 0 || bb.capacity() < size) {
				state = State.ERROR;
				return ProcessStatus.ERROR;
			}
		}

		bb.flip();
		try {
			if (bb.remaining() < size) {
				return ProcessStatus.REFILL;
			}

			var limit = bb.limit();
			bb.limit(size);
			string = charset.decode(bb).toString();

			bb.limit(limit);

			state = State.DONE;
			return ProcessStatus.DONE;
		} finally {
			bb.compact();
		}
	}

	/**
	 * Get a string
	 */
	@Override
	public Object get() {
		if (state != State.DONE) {
			throw new IllegalStateException();
		}
		return string;
	}

	/**
	 * reset the reader
	 */
	@Override
	public void reset() {
		intReader.reset();
		state = State.WAITING_INT;
	}

}
