package fr.upem.chaton.reader.atomic;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.chaton.ProcessStatus;

public class OpReader implements AtomicReader {
	private enum State {
		DONE, WAITING, ERROR
	};

	private final ByteBuffer bb;
	private State state = State.WAITING;
	private byte value;

	public OpReader(ByteBuffer bb) {
		this.bb = Objects.requireNonNull(bb);
	}

	/**
	 * Read a byte if it's possibleand return the ProcessStatus corresponding
	 * @return ProcessStatus
	 */
	@Override
	public ProcessStatus process() {
		if (state == State.DONE || state == State.ERROR) {
			throw new IllegalStateException();
		}
		bb.flip();
		try {
			if (bb.remaining() >= Byte.BYTES) {
				value = bb.get();
				state = State.DONE;
				return ProcessStatus.DONE;
			} else {
				return ProcessStatus.REFILL;
			}
		} finally {
			bb.compact();
		}

	}

	/**
	 * Get a byte
	 */
	@Override
	public Object get() {
		if (state != State.DONE) {
			throw new IllegalStateException();
		}

		return value;
	}

	/**
	 * reset the reader
	 */
	@Override
	public void reset() {
		state = State.WAITING;
	}
}
