package fr.upem.chaton.reader;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import fr.upem.chaton.protocole.Protocole.Atom;
import fr.upem.chaton.reader.atomic.AtomicReader;
import fr.upem.chaton.reader.atomic.IntReader;
import fr.upem.chaton.reader.atomic.LongReader;
import fr.upem.chaton.reader.atomic.OpReader;
import fr.upem.chaton.reader.atomic.StringReader;

public class DataReaderBuilder {
	private List<Atom> config;
	private ByteBuffer bb;
	private Charset charset;

	/**
	 * set the config request protocole
	 * @param config
	 * @return
	 */
	public DataReaderBuilder setProtocol(List<Atom> config) {
		Objects.requireNonNull(config);
		this.config = config;
		return this;
	}

	/**
	 * set the bytebuffer
	 * @param bb
	 * @return
	 */
	public DataReaderBuilder setBB(ByteBuffer bb) {
		Objects.requireNonNull(bb);
		this.bb = bb;
		return this;
	}

    /**
     * set the charset
     */
	public DataReaderBuilder setCharset(Charset charset) {
		Objects.requireNonNull(charset);
		this.charset = charset;
		return this;
	}

	/**
     * Build the list of readers atomic
     */
	public List<AtomicReader> build() {
		List<AtomicReader> toDo = new ArrayList<>();

		for (var type : config) {
			switch (type) {
			case OP:
				toDo.add(new OpReader(bb));
				break;
			case INT:
				toDo.add(new IntReader(bb));
				break;
			case LONG:
				toDo.add(new LongReader(bb));
				break;
			case STRING:
				toDo.add(new StringReader(bb, charset));
				break;
			default:
				throw new IllegalArgumentException("Don't recognize Protocole's config");
			}
		}
		return toDo;
	}
}
