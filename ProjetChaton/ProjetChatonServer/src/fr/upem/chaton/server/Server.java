package fr.upem.chaton.server;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.CancelledKeyException;
import java.nio.channels.Channel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.logging.Logger;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.reader.DataReader;
import fr.upem.chaton.server.database.DataBase;
import fr.upem.chaton.writer.DataWriter;

/* A G�rer la queue pour pas d�passer une certaine quantité de donnée */ 
public class Server {
	static class ServerContext {
		public static final int MAX_ELEMENT_QUEUE = 100;
		
		final private Server server;
		final private DataBase dataBase;
		final private SelectionKey key;
		final private SocketChannel sc;
		final private ByteBuffer bbin = ByteBuffer.allocate(BUFFER_SIZE);
		final private ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);
		final private Queue<DataChaton> queue = new ArrayDeque<>(MAX_ELEMENT_QUEUE);
		final private Charset UTF8 = Charset.forName("UTF8");
		final private DataReader reader = new DataReader(bbin, UTF8);
		final private DataWriter writer = new DataWriter(bbout, UTF8);

		private ServerContextInterface context;
		private boolean closed = false;

		private ServerContext(Server server, DataBase dataBase, Selector selector, SelectionKey key) {

			this.key = key;
			this.sc = (SocketChannel) key.channel();
			this.server = server;
			this.dataBase = dataBase;
			context = new ServerContextStarter(server, dataBase, key, reader, writer);
		}

		void updateToContextPrivate(ServerContext foreign, String token) {
			context = new ServerContextPrivate(dataBase.tokens.getTokenInfo(token), bbin, foreign.bbout, server, foreign.key);
		}

		void updateToContextPublic(String login) {
			dataBase.logins.register(login, key);
			context = new ServerContextPublic(server, dataBase, login, key, reader, writer);
		}

		private void processIn() {
			for (;;) {
				var status = context.processIn(); // A mettre dans les contexts
				switch (status) {
				case DONE:
					break;
				case REFILL:
					return;
				case ERROR:
					closed = true;
					return;
				}
			}
		}

		private void queueMessage(DataChaton msg) {
			if(msg == null) {
				updateInterestOps();
				return;
			}
			if(queue.size() < MAX_ELEMENT_QUEUE) {
				queue.add(msg);
				processOut();
			}
			else {
				closed = true;
			}
			updateInterestOps();
		}

		private void processOut() {
			while (!queue.isEmpty()) {
				var data = queue.peek();
				var status = context.processOut(data);
				switch (status) {
				case DONE:
					queue.poll();
					break;
				case REFILL:
					return;
				case ERROR:
					System.out.println("error out");
					closed = true;
					return;
				}
			}
		}

		private void updateInterestOps() {
			int newOp = 0;

			if (bbin.hasRemaining() && !closed) {
				newOp |= SelectionKey.OP_READ;
			}
			if (bbout.position() != 0) {
				newOp |= SelectionKey.OP_WRITE;
			}

			if (newOp == 0) {
				silentlyClose();
			}
			key.interestOps(newOp);

		}

		private void silentlyClose() {
			try {
				context.silentlyClose();
				sc.close();
			} catch (IOException e) {
			}
		}

		private void doRead() throws IOException {
			if (sc.read(bbin) == -1) {
				logger.info("Closed is set to true");
				closed = true;
			}
			processIn();
			updateInterestOps();
		}

		private void doWrite() throws IOException {
			bbout.flip();
			sc.write(bbout);
			bbout.compact();
			updateInterestOps();
		}
	}


	//static private final int MAX_CLIENT = 100;
	static private int BUFFER_SIZE = 1_024;
	static private Logger logger = Logger.getLogger(Server.class.getName());

	private final ServerSocketChannel serverSocketChannel;
	private final Selector selector;
	private final DataBase dataBase;

	public Server(int port) throws IOException {
		selector = Selector.open();
		serverSocketChannel = ServerSocketChannel.open();
		serverSocketChannel.bind(new InetSocketAddress(port));
		serverSocketChannel.configureBlocking(false);
		serverSocketChannel.register(selector, SelectionKey.OP_ACCEPT);
		dataBase = new DataBase(this);
	}

	public void launch() throws IOException {
		logger.info("Starting server ...............");
		while (!Thread.interrupted()) {
			try {
				selector.select(this::treatKey);
			} catch (UncheckedIOException tunneled) {
				throw tunneled.getCause();
			}
		}
	}

	private void treatKey(SelectionKey key) {
		// DebugToolsSelector.printKeys(selector);
		try {
			
			if (key.isValid() && key.isAcceptable()) {
				doAccept(key);
			}
		} catch (IOException ioe) {
			throw new UncheckedIOException(ioe);
		}
		try {
			if (key.isValid() && key.isWritable()) {
				((ServerContext) key.attachment()).doWrite();
			}
			if (key.isValid() && key.isReadable()) {
				((ServerContext) key.attachment()).doRead();
			}
		} catch (IOException | CancelledKeyException e) {
			logger.info("Connection closed with client due to IOException");
			silentlyClose(key);
		} 
	}

	private void doAccept(SelectionKey key) throws IOException {
		var ssc = (ServerSocketChannel) key.channel();
		var sc = ssc.accept();
		sc.configureBlocking(false);
		var newKey = sc.register(selector, SelectionKey.OP_READ);
		var newContext = new ServerContext(this, dataBase, selector, newKey);
		newKey.attach(newContext);
	}

	private void silentlyClose(SelectionKey key) {
		Channel sc = (Channel) key.channel();
		try {
			((ServerContext)key.attachment()).context.silentlyClose();
			sc.close();
		} catch (IOException e) {
		}
	}

	/**
	 * send the message for all client in public connection
	 * @param msg
	 */
	public void broadcast(DataChaton msg) {
		selector.keys().forEach(key -> {
			var channel = key.channel();
			if (channel != (ServerSocketChannel) serverSocketChannel) {
				var context = (ServerContext) key.attachment();
				if(context.context instanceof ServerContextPublic)
					context.queueMessage(msg);
			}
		});
	}

	/**
	 * send the message for the client given
	 * @param key
	 * @param msg
	 */
	public void send(SelectionKey key, DataChaton msg) {
		var channel = key.channel();
		if (channel != (ServerSocketChannel) serverSocketChannel) {
			var context = (ServerContext) key.attachment();
			context.queueMessage(msg);
		}
	}
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		if (args.length != 1) {
			usage();
			return;
		}
		new Server(Integer.parseInt(args[0])).launch();
	}

	private static void usage() {
		System.out.println("Usage : ServerChaton port");
	}
}
