package fr.upem.chaton.server.database;

import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import fr.upem.chaton.server.Server;
import fr.upem.chaton.server.ServerMessage;
import fr.upem.chaton.server.database.TokenHandler.TokenInfo;

public class UserHandler {
	private static final Logger logger = Logger.getLogger(UserHandler.class.getName());
	private final HashMap<String, ClientInfo> users = new HashMap<>();

	public static class ClientInfo {
		private final String clientLogin;
		private final SelectionKey clientKey;
		private int countPrivateConnection = 0;
		private final List<ClientInfo> waitingPrivateConnections = new ArrayList<>(); // List foreigns
		private final List<TokenInfo> privateConnections = new ArrayList<>();
		private final UserHandler usersDataBase;
		private final List<ServerClientStatus> status = new ArrayList<>();

		private ClientInfo(String clientLogin, SelectionKey clientKey, UserHandler usersDataBase) {
			this.clientLogin = clientLogin;
			this.clientKey = clientKey;
			this.usersDataBase = usersDataBase;
		}
		
		/**
		 * add a new status
		 * @param status
		 */
		public void addStatus(ServerClientStatus status) {
			this.status.add(status);
		}
		
		/**
		 * remove a new status
		 * @param status
		 */
		public boolean removeStatus(ServerClientStatus status) {
			return this.status.remove(status);
		}
		
		/**
		 * get all new status
		 * @param status
		 */
		public List<ServerClientStatus> getAllStatus() {
			return status;
		}
		
		/**
		 * add a new client in waitingprivateconnection
		 * @param status
		 */
		
		public boolean addWaitingPrivateConnection(String foreignLogin) {
			Objects.requireNonNull(foreignLogin);
			
			ClientInfo foreignInfo = usersDataBase.getClientInfo(foreignLogin);
			
			if(foreignInfo == null) {
				logger.info("The foreign is not connected : " + foreignLogin);
				return false;
			}
			
			
			for(var foreign : waitingPrivateConnections) { // Check if the client have already a connection with the foreign client
				if(foreign == foreignInfo) {
					logger.info("The foreign is already in waitingPrivateConnections : " + foreignLogin);
					return false;
				}
			}
			
			for(var token : privateConnections) { // Check if the client have already a connection with the foreign client
				for(var foreign : token.getUsers()) {
					if(foreign == foreignInfo) {
						logger.info("The foreign is already in privateConnections : " + foreignLogin);
						return false;
					}
				}
			}
			
			if (countPrivateConnection < DataBase.MAX_PRIVATE_CONNECTION
				&& foreignInfo.countPrivateConnection < DataBase.MAX_PRIVATE_CONNECTION) {
				
				waitingPrivateConnections.add(foreignInfo);
				countPrivateConnection++;
				foreignInfo.waitingPrivateConnections.add(this);
				foreignInfo.countPrivateConnection++;
				return true;
			}

			logger.info("One or both client have max private connection");
			return false;
		}
		
		/**
		 * add a new private connection
		 * @param status
		 */
		
		public void addPrivateConnection(TokenInfo token) {
			Objects.requireNonNull(token);
			privateConnections.add(token);
		}
		
		/**
		 * update the waiting private connection
		 * @param status
		 */
		
		public boolean updateWaitingPrivateConnection(TokenInfo tokenInfo, String foreignLogin) {
			Objects.requireNonNull(foreignLogin);
			
			ClientInfo foreign = usersDataBase.getClientInfo(foreignLogin);

			if (foreign != null 
				&& countPrivateConnection <= DataBase.MAX_PRIVATE_CONNECTION
				&& foreign.countPrivateConnection <= DataBase.MAX_PRIVATE_CONNECTION) {

				if (this.removeWaitingPrivateConnection(foreignLogin)) {
					countPrivateConnection++;
					foreign.countPrivateConnection++;
					return true;
				}
			}
			logger.info("One or both client have max private connection");
			return false;
		}
		
		/**
		 * remove the waiting private connection
		 * @param status
		 */
		
		public boolean removeWaitingPrivateConnection(String foreignLogin) {
			Objects.requireNonNull(foreignLogin);
			
			ClientInfo foreign = usersDataBase.getClientInfo(foreignLogin);
			
			if(foreign == null) {
				return false;
			}
			
			var lClient = waitingPrivateConnections;
			var lForeign = foreign.waitingPrivateConnections;

			if (lClient.contains(foreign) && lForeign.contains(this)) {
				lClient.remove(foreign);
				lForeign.remove(this);
				countPrivateConnection--;
				foreign.countPrivateConnection--;
				return true;
			}
			return false;
		}

		private void removeWaitingPrivateConnections(Server server) {
			for(var foreign : waitingPrivateConnections) {
				var lForeign = foreign.waitingPrivateConnections;
				lForeign.remove(this);
				server.send(foreign.clientKey, ServerMessage.refuseResponse(""));
				foreign.countPrivateConnection--;
			}
		}
		
		/**
		 * remove the private connection
		 * @param status
		 */
		
		public void removePrivateConnection(TokenInfo token) {
			Objects.requireNonNull(token);
			privateConnections.remove(token);
			countPrivateConnection--;
		}
		
		private void removePrivateConnections() {
			List<TokenInfo> tokensToRemove = new ArrayList<>();
			for (TokenInfo token : privateConnections) {
				tokensToRemove.add(token);
			}
			
			for(TokenInfo token : tokensToRemove) {
				for (ClientInfo user : token.getUsers()) {
					if (user == this)
						continue;
					user.removePrivateConnection(token);
				}
				token.removeToken();
			}
			
		}
		
		/**
		 * remove the user
		 * @param status
		 */
		public boolean removeUser(Server server) { 
			logger.info("Removing User : " + clientLogin );
			removeWaitingPrivateConnections(server);
			removePrivateConnections();
			usersDataBase.users.remove(clientLogin);
			return true;
		}
	}

	/**
	 * add a new user in the dataBase
	 * @param status
	 */
	public void register(String clientLogin, SelectionKey clientKey) {
		Objects.requireNonNull(clientLogin);
		Objects.requireNonNull(clientKey);
		var user = new ClientInfo(clientLogin, clientKey, this);
		users.put(clientLogin, user);
	}

	/**
	 * Check is the login is present in the dataBase
	 * @param clientLogin
	 * @return
	 */
	public boolean isPresent(String clientLogin) {
		Objects.requireNonNull(clientLogin);
		if(users.containsKey(clientLogin)) {
			return true;
		}
		logger.info("The client " + clientLogin + " is not connected");
		return false;
	}

	/**
	 * Get the user info
	 * @param login
	 * @return
	 */
	public ClientInfo getClientInfo(String login) {
		Objects.requireNonNull(login);
		return users.get(login);
	}

	/**
	 * get the slectionkey of the user
	 * @param clientLogin
	 * @return
	 */
	public SelectionKey getSelectionKey(String clientLogin) {
		Objects.requireNonNull(clientLogin);
		var client = users.get(clientLogin);
		return (client != null) ? client.clientKey : null;
	}
}
