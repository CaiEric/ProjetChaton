package fr.upem.chaton.server.database;

public enum ServerClientStatus {
	WAITING, WAITING_AUTHENTIFICATION, WAITING_PRIVATE_CONNECTION_RESPONSE
}
