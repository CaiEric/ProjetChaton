package fr.upem.chaton.server.database;

import java.io.IOException;
import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

import fr.upem.chaton.server.database.UserHandler.ClientInfo;

public class TokenHandler {
	private static final Logger logger = Logger.getLogger(TokenHandler.class.getName());
	private final HashMap<String, TokenInfo> tokens = new HashMap<>();

	public static class TokenInfo {
		private final String token;
		private final List<SelectionKey> privateKeys = new ArrayList<>(DataBase.MAX_USER);
		private final List<ClientInfo> users = new ArrayList<>(DataBase.MAX_USER);
		private final TokenHandler tokensDataBase;

		private TokenInfo(String token, TokenHandler tokensDataBase) {
			this.token = token;
			this.tokensDataBase = tokensDataBase;
		}

		/**
		 * add a new private key
		 * @param key
		 * @return
		 */
		public boolean addPrivateKey(SelectionKey key) {
			Objects.requireNonNull(key);

			if (privateKeys.size() < DataBase.MAX_USER) {
				privateKeys.add(key);
				return true;
			}
			return false;
		}
		
		private void addUser(ClientInfo client) {
			Objects.requireNonNull(client);
			users.add(client);
			client.addPrivateConnection(this);
		}
		
		/** 
		 * Check if the user wait his pair for finish the private connection establishment
		 * @return
		 */
		public boolean isWaiting() {
			return (privateKeys.size() < DataBase.MAX_USER) ? true : false;
		}
		
		
		/**
		 * get the token
		 * @return
		 */
		public String getToken() {
			return token;
		}

		/**
		 * get the keys for the private connections
		 * @return
		 */
		public List<SelectionKey> getPrivateKeys() {
			return privateKeys;
		}

		/**
		 * get users
		 * @param status
		 */
		public List<ClientInfo> getUsers() {
			return users;
		}
		
		
		private void closePrivateKeys() {
			for (SelectionKey key : privateKeys) {
				try {key.channel().close();} 
				catch (IOException e) {}
			}
		}
		
		/** 
		 * remove Token and close private keyss
		 */
		public void removeToken() {
			logger.info("Removing Token : " + token);
			for(var user : users) {
				user.removePrivateConnection(this);
			}
			closePrivateKeys();
			tokensDataBase.tokens.remove(token);
		}

	}

	/**
	 * generate a token which is unique
	 * @return
	 */
	public TokenInfo generateNewToken() {
		long token;
		do {
			token = System.currentTimeMillis();
		} while (isPresent(String.valueOf(token)));

		var tokenInfo = new TokenInfo(String.valueOf(token), this);
		return tokenInfo;
	}

	/**
	 * register a token in the dataBase
	 * @param tokenInfo
	 * @param users
	 * @return
	 */
	public TokenInfo register(TokenInfo tokenInfo, List<ClientInfo> users) {
		Objects.requireNonNull(tokenInfo);
		Objects.requireNonNull(users);

		if (isPresent(tokenInfo.getToken()))
			throw new IllegalArgumentException();

		if (users.size() != DataBase.MAX_USER) {
			throw new IllegalArgumentException();
		}

		tokens.put(tokenInfo.getToken(), tokenInfo);
		
		users.forEach(user -> tokenInfo.addUser(user));

		return tokenInfo;
	}

	/**
	 * check if a token is present in the database
	 * @param token
	 * @return
	 */
	public boolean isPresent(String token) {
		Objects.requireNonNull(token);
		return tokens.containsKey(token);
	}

	/**
	 * remove the token
	 * @param token
	 * @return
	 */
	public boolean removeTokenInfo(String token) {
		Objects.requireNonNull(token);
		var tk = tokens.remove(token);
		if (tk != null) {
			return true;
		}
		return false;
	}

	/** 
	 * get the token info
	 * @param token
	 * @return
	 */
	public TokenInfo getTokenInfo(String token) {
		Objects.requireNonNull(token);
		var tk = tokens.get(token);
		if (tk != null)
			return tk;
		throw new IllegalArgumentException();
	}
}
