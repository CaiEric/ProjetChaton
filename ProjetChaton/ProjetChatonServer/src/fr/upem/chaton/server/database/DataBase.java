package fr.upem.chaton.server.database;

import fr.upem.chaton.server.Server;

public class DataBase {

	public static final int MAX_USER = 2;
	public static final int MAX_PRIVATE_CONNECTION = 5;
	
	public final TokenHandler tokens = new TokenHandler();
	public final UserHandler logins = new UserHandler();
	public final Server server;

	public DataBase(Server server) {
		this.server = server;
	}

}
