package fr.upem.chaton.server.valid;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.function.BiPredicate;
import java.util.logging.Logger;

import fr.upem.chaton.protocole.TypeRequest;
import fr.upem.chaton.server.database.ServerClientStatus;

public class RequestValidator {

	static private Logger logger = Logger.getLogger(RequestValidator.class.getName());

	/**
	 * The method is use to test a request with a status given
	 * @param status
	 * @param request
	 * @param predicate
	 * @return boolean
	 */
	public static boolean test(List<ServerClientStatus> status, TypeRequest request,
			BiPredicate<List<ServerClientStatus>, TypeRequest> predicate) {
		Objects.requireNonNull(status);
		Objects.requireNonNull(request);
		Objects.requireNonNull(predicate);
		return predicate.test(status, request);
	}

	/**
	 * Test if the authentification request is valid with the status given
	 * @param status
	 * @param request
	 * @return boolean
	 */
	public static boolean validAuthentificationRequest(List<ServerClientStatus> status, TypeRequest request) {
		if (!Arrays.asList(TypeRequest.AUTHENTIFICATION, TypeRequest.TOKEN_CLIENT).contains(request))
			throw new IllegalArgumentException();

		if (status.contains(ServerClientStatus.WAITING_AUTHENTIFICATION))
			return true;
		logger.info("The client try to request without authentification");
		return false;
	}

	/**
	 * Test if the request is valid with the status given
	 * @param status
	 * @param request
	 * @return boolean
	 */
	public static boolean validAlreadyConnectedRequest(List<ServerClientStatus> status, TypeRequest request) {
		if (Arrays.asList(TypeRequest.AUTHENTIFICATION, TypeRequest.TOKEN_CLIENT).contains(request))
			throw new IllegalArgumentException();

		if (!status.contains(ServerClientStatus.WAITING_AUTHENTIFICATION))
			return true;
		logger.info("Client with this login is already connected");
		return false;
	}

	/**
	 * Test if the private connection request is valid with the status given
	 * @param status
	 * @param request
	 * @return boolean
	 */
	
	public static boolean validPrivateConnectionResponseRequest(List<ServerClientStatus> status, TypeRequest request) {
		if (!Arrays.asList(TypeRequest.ACCEPT, TypeRequest.REFUSE).contains(request))
			throw new IllegalArgumentException();

		if (status.contains(ServerClientStatus.WAITING_PRIVATE_CONNECTION_RESPONSE)) {
			return true;
		}
		logger.info("The client give incoherent request, the server doesn't wait a accept or refuse response");
		return false;
	}
}
