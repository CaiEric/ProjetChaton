package fr.upem.chaton.server;

import java.nio.channels.SelectionKey;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.logging.Logger;

import fr.upem.chaton.ProcessStatus;
import fr.upem.chaton.protocole.TypeRequest;
import fr.upem.chaton.reader.DataReader;
import fr.upem.chaton.server.database.ServerClientStatus;
import fr.upem.chaton.server.database.DataBase;
import fr.upem.chaton.server.database.TokenHandler;
import fr.upem.chaton.server.database.UserHandler;
import fr.upem.chaton.server.database.UserHandler.ClientInfo;
import fr.upem.chaton.server.valid.RequestValidator;
import fr.upem.chaton.writer.DataWriter;

public class ServerContextPublic extends ServerContextAbstract {
	private static final Logger logger = Logger.getLogger(ServerContextPublic.class.getName());
	private final ClientInfo clientInfo;
	private final String clientLogin;
	private final List<ServerClientStatus> status;
	private final UserHandler usersDataBase;
	private final TokenHandler tokensDataBase;

	public ServerContextPublic(Server server, DataBase dataBase, String clientLogin, SelectionKey clientKey,
			DataReader reader, DataWriter writer) {
		super(server, dataBase, clientKey, reader, writer);
		this.clientLogin = clientLogin;
		this.clientInfo = dataBase.logins.getClientInfo(clientLogin);
		this.clientInfo.addStatus(ServerClientStatus.WAITING);
		this.status = clientInfo.getAllStatus();
		this.usersDataBase = dataBase.logins;
		this.tokensDataBase = dataBase.tokens;
	}

	/**
	 * process the request in read 
	 */
	ProcessStatus processRequest() {

		switch (request) {
		case PRIVATE_MESSAGE:
			return processSendPrivateMessage(RequestValidator::validAlreadyConnectedRequest);
		case PUBLIC_MESSAGE:
			return processMessageAll(RequestValidator::validAlreadyConnectedRequest);
		case PRIVATE_CONNECTION:
			return processPrivateConnection(RequestValidator::validAlreadyConnectedRequest);
		case ACCEPT:
			return processAcceptPrivateConnection(RequestValidator::validPrivateConnectionResponseRequest);
		case REFUSE:
			return processRefusePrivateConnection(RequestValidator::validPrivateConnectionResponseRequest);
		default:
			return ProcessStatus.ERROR;
		}
	}

	private ProcessStatus processSendPrivateMessage(BiPredicate<List<ServerClientStatus>, TypeRequest> valid) {

		var foreignLogin = data.poll();
		var foreignKey = usersDataBase.getSelectionKey(foreignLogin);
		var message = data.poll();

		if (!valid.test(status, request)) {
			return ProcessStatus.ERROR;
		}

		if(clientLogin.equals(foreignLogin)) {
			logger.info("Warning : the client " + clientLogin + " send a message for himself");
		}
		
		if (!usersDataBase.isPresent(foreignLogin)) {
			return ProcessStatus.DONE;
		}

		logger.info(clientLogin + " send to " + foreignLogin + ", Message : " + message);
		server.send(foreignKey, ServerMessage.sendPrivateMessage(clientLogin, message));

		return ProcessStatus.DONE;
	}

	private ProcessStatus processMessageAll(BiPredicate<List<ServerClientStatus>, TypeRequest> valid) {

		var login = data.poll();
		var message = data.poll();

		if (!valid.test(status, request)) {
			return ProcessStatus.ERROR;
		}
		
		if(!usersDataBase.isPresent(login)) {
			return ProcessStatus.ERROR;
		}

		logger.info("Sending the following message to all : " + message);
		server.broadcast(ServerMessage.sendMessage(login, message));

		return ProcessStatus.DONE;
	}

	private ProcessStatus processPrivateConnection(BiPredicate<List<ServerClientStatus>, TypeRequest> valid) {

		var foreignLogin = data.poll();
		var foreignKey = usersDataBase.getSelectionKey(foreignLogin);
		var foreignClient = usersDataBase.getClientInfo(foreignLogin);
		
		if (!valid.test(status, request)) {
			return ProcessStatus.ERROR;
		}

		if(clientLogin.equals(foreignLogin)) {
			logger.info("The client can't create a private connection with himself");
			return ProcessStatus.ERROR;
		}
		
		if(!usersDataBase.isPresent(foreignLogin)) {
			server.send(clientKey, ServerMessage.refuseResponse(foreignLogin));
			return ProcessStatus.DONE;
		}
		
		if (!clientInfo.addWaitingPrivateConnection(foreignLogin)) {
			server.send(clientKey, ServerMessage.refuseResponse(foreignLogin));
			return ProcessStatus.DONE;
		}

		logger.info(clientLogin + " try to establish a private connection with " + foreignLogin);
		server.send(foreignKey, ServerMessage.askPrivateConnection(clientLogin));

		foreignClient.getAllStatus().add(ServerClientStatus.WAITING_PRIVATE_CONNECTION_RESPONSE);
		return ProcessStatus.DONE;
	}

	private ProcessStatus processAcceptPrivateConnection(BiPredicate<List<ServerClientStatus>, TypeRequest> valid) {
		
		var foreignLogin = data.poll();
		var foreignInfo = usersDataBase.getClientInfo(foreignLogin);
		var foreignKey = usersDataBase.getSelectionKey(foreignLogin);
		var tokenInfo = tokensDataBase.generateNewToken();
		
		if (!valid.test(status, request)) {
			return ProcessStatus.ERROR;
		}
		
		if (!usersDataBase.isPresent(foreignLogin)) {
			return ProcessStatus.ERROR;
		}
		
		if (!clientInfo.updateWaitingPrivateConnection(tokenInfo, foreignLogin)) {
			return ProcessStatus.ERROR;
		}
		
		tokensDataBase.register(tokenInfo, Arrays.asList(clientInfo, foreignInfo));

		logger.info(foreignLogin + " accept to create a private connection with " + clientLogin);
		server.send(foreignKey, ServerMessage.acceptResponse(clientLogin));
		server.send(clientKey, ServerMessage.sendToken(tokenInfo.getToken(), foreignLogin));
		server.send(foreignKey, ServerMessage.sendToken(tokenInfo.getToken(), clientLogin));

		status.remove(ServerClientStatus.WAITING_PRIVATE_CONNECTION_RESPONSE);
		return ProcessStatus.DONE;
	}

	private ProcessStatus processRefusePrivateConnection(BiPredicate<List<ServerClientStatus>, TypeRequest> valid) {

		var foreignLogin = data.poll();
		var foreignKey = usersDataBase.getSelectionKey(foreignLogin);

		if (!valid.test(status, request)) {
			return ProcessStatus.ERROR;
		}

		if (!usersDataBase.isPresent(foreignLogin)) {
			return ProcessStatus.ERROR;
		}
		
		if (!clientInfo.removeWaitingPrivateConnection(foreignLogin)) {
			return ProcessStatus.ERROR;
		}

		logger.info(foreignLogin + " refuse to create a private connection with " + clientLogin);
		server.send(foreignKey, ServerMessage.refuseResponse(clientLogin));

		status.remove(ServerClientStatus.WAITING_PRIVATE_CONNECTION_RESPONSE);
		return ProcessStatus.DONE;
	}

	/** 
	 * update the dataBase 
	 */
	public void silentlyClose() {
		clientInfo.removeUser(server);
	}
}
