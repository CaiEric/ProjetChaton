package fr.upem.chaton.server;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.ProcessStatus;

public interface ServerContextInterface {

	ProcessStatus processIn();

	ProcessStatus processOut(DataChaton data);

	void silentlyClose();
}
