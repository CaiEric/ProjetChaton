package fr.upem.chaton.server;

import java.util.Objects;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.protocole.TypeRequest;

public class ServerMessage {

	/**
	 * accept Message
	 * @param file
	 * @return
	 */
	public static DataChaton acceptResponse(String message) {
		Objects.requireNonNull(message);
		var data = new DataChaton();
		data.setType(TypeRequest.ACCEPT);
		data.add(String.valueOf(TypeRequest.ACCEPT.ordinal()));
		data.add(message);

		return data;
	}

	/**
	 * refuse Message
	 * @param file
	 * @return
	 */
	public static DataChaton refuseResponse(String message) {
		Objects.requireNonNull(message);
		var data = new DataChaton();
		data.setType(TypeRequest.REFUSE);
		data.add(String.valueOf(TypeRequest.REFUSE.ordinal()));
		data.add(message);

		return data;
	}

	/**
	 * private Message
	 * @param file
	 * @return
	 */
	public static DataChaton sendPrivateMessage(String login, String message) {
		Objects.requireNonNull(login);
		Objects.requireNonNull(message);
		var data = new DataChaton();
		data.setType(TypeRequest.PRIVATE_MESSAGE);
		data.add(String.valueOf(TypeRequest.PRIVATE_MESSAGE.ordinal()));
		data.add(login);
		data.add(message);
		return data;
	}

	/**
	 * public Message
	 * @param file
	 * @return
	 */
	public static DataChaton sendMessage(String login, String message) {
		Objects.requireNonNull(message);
		var data = new DataChaton();
		data.setType(TypeRequest.PUBLIC_MESSAGE);
		data.add(String.valueOf(TypeRequest.PUBLIC_MESSAGE.ordinal()));
		data.add(login);
		data.add(message);
		return data;
	}

	/**
	 * private connection Message
	 * @param file
	 * @return
	 */
	public static DataChaton askPrivateConnection(String login) {
		Objects.requireNonNull(login);
		var data = new DataChaton();
		data.setType(TypeRequest.PRIVATE_CONNECTION);
		data.add(String.valueOf(TypeRequest.PRIVATE_CONNECTION.ordinal()));
		data.add(login);
		return data;
	}

	/**
	 * token Message
	 * @param file
	 * @return
	 */
	
	public static DataChaton sendToken(String token, String login) {
		Objects.requireNonNull(token);
		Objects.requireNonNull(login);
		var data = new DataChaton();
		data.setType(TypeRequest.TOKEN_SERVER);
		data.add(String.valueOf(TypeRequest.TOKEN_SERVER.ordinal()));
		data.add(token);
		data.add(login);
		return data;
	}
}
