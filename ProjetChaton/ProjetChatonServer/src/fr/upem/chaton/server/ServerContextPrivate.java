package fr.upem.chaton.server;

import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.util.Objects;
import java.util.logging.Logger;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.ProcessStatus;
import fr.upem.chaton.server.database.TokenHandler.TokenInfo;

public class ServerContextPrivate implements ServerContextInterface {

	private static final Logger logger = Logger.getLogger(ServerContextPrivate.class.getName());
	
	private ByteBuffer bbinA;
	private ByteBuffer bboutB;
	private final SelectionKey foreignKey;
	private final Server server;
	private final TokenInfo tokenInfo;

	public ServerContextPrivate(TokenInfo tokenInfo, ByteBuffer bbinA, ByteBuffer bboutB, Server server, SelectionKey foreignKey) {
		Objects.requireNonNull(bbinA);
		Objects.requireNonNull(bboutB);

		this.bbinA = bbinA;
		this.bboutB = bboutB;
		this.foreignKey = foreignKey;
		this.server = server;
		this.tokenInfo = tokenInfo;
	}

	/**
	 * process the request in read 
	 */
	@Override
	public ProcessStatus processIn() {
		
		logger.info("Process in from private channels between :");
		System.out.println(bbinA);
		System.out.println(bboutB);

		bbinA.flip();
		try {
			if(!bbinA.hasRemaining()) {
				logger.info("REFILL");
				return ProcessStatus.REFILL;
			}

			bboutB.put(bbinA);
			
			if(!bbinA.hasRemaining()) {
				return ProcessStatus.REFILL;
			}
			return ProcessStatus.DONE;
		} finally {
			bbinA.compact();
			server.send(foreignKey, null);
		}
	}

	/**
	 * process the request out read 
	 */
	@Override
	public ProcessStatus processOut(DataChaton data) {
		logger.info("Process out from private channels between :");
		return ProcessStatus.DONE;
	}

	/** 
	 * update the database
	 */
	public void silentlyClose() {
		tokenInfo.removeToken();
		return;
	}
}
