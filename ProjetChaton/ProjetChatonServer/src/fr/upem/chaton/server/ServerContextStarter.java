package fr.upem.chaton.server;

import java.nio.channels.SelectionKey;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.BiPredicate;
import java.util.logging.Logger;

import fr.upem.chaton.ProcessStatus;
import fr.upem.chaton.protocole.TypeRequest;
import fr.upem.chaton.reader.DataReader;
import fr.upem.chaton.server.Server.ServerContext;
import fr.upem.chaton.server.database.ServerClientStatus;
import fr.upem.chaton.server.database.DataBase;
import fr.upem.chaton.server.database.TokenHandler;
import fr.upem.chaton.server.database.UserHandler;
import fr.upem.chaton.server.valid.RequestValidator;
import fr.upem.chaton.writer.DataWriter;

public class ServerContextStarter extends ServerContextAbstract {
	private static final Logger logger = Logger.getLogger(ServerContextStarter.class.getName());

	private final UserHandler usersDataBase;
	private final TokenHandler tokensDataBase;
	private final List<ServerClientStatus> status = new ArrayList<>(Arrays.asList(ServerClientStatus.WAITING_AUTHENTIFICATION)); 
	// Use for check if the request is allowed
	
	public ServerContextStarter(Server server, DataBase dataBase, SelectionKey clientKey, DataReader reader,
			DataWriter writer) {
		super(server, dataBase, clientKey, reader, writer);

		this.usersDataBase = dataBase.logins;
		this.tokensDataBase = dataBase.tokens;
	}

	/**
	 * process the request in read 
	 */
	ProcessStatus processRequest() {
		switch (request) {
		case AUTHENTIFICATION:
			return processConnection(RequestValidator::validAuthentificationRequest);
		case TOKEN_CLIENT:
			return processTokenClient(RequestValidator::validAuthentificationRequest);
		default:
			logger.info("The client send an invalid request : " + request);
			return ProcessStatus.ERROR;
		}
	}

	private ProcessStatus processConnection(BiPredicate<List<ServerClientStatus>, TypeRequest> valid) {

		var login = data.poll();

		if (!valid.test(status, request)) {
			logger.info("The user give incorrect request");
			return ProcessStatus.ERROR;
		}

		if (usersDataBase.isPresent(login)) {
			logger.info("Client with this login is already connected");
			server.send(clientKey, ServerMessage.refuseResponse("Une autre personne s'est déjà connecté avec ce login."));
			return ProcessStatus.DONE;
		}

		logger.info("Connection of a new client");
		server.send(clientKey, ServerMessage.acceptResponse("Vous venez de vous connecter."));

		((ServerContext) clientKey.attachment()).updateToContextPublic(login);
		
		return ProcessStatus.DONE;
	}

	private ProcessStatus processTokenClient(BiPredicate<List<ServerClientStatus>, TypeRequest> valid) {

		var token = data.poll();

		if (!valid.test(status, request)) {
			logger.info("The user give incorrect request");
			return ProcessStatus.ERROR;
		}

		if (!tokensDataBase.isPresent(token)) {
			logger.info("The token given by the client is not valid.");
			return ProcessStatus.ERROR;
		}
		
		var tokenInfo = tokensDataBase.getTokenInfo(token);
		if (!tokenInfo.addPrivateKey(clientKey)) {
			logger.info("Max channels connected with this token");
			return ProcessStatus.ERROR;
		}

		if (tokenInfo.isWaiting()) {
			logger.info("A client is waiting for the private connection.");
			return ProcessStatus.DONE;
		}

		var lKeys = tokenInfo.getPrivateKeys();
		var key1 = lKeys.get(0);
		var key2 = lKeys.get(1);

		logger.info("The private connection is established.");
		server.send(key1, ServerMessage.acceptResponse("Connexion privé établi"));
		server.send(key2, ServerMessage.acceptResponse("Connexion privé établi"));
		((ServerContext) key1.attachment()).updateToContextPrivate(((ServerContext) key2.attachment()), token);
		((ServerContext) key2.attachment()).updateToContextPrivate(((ServerContext) key1.attachment()), token);

		return ProcessStatus.DONE;
	}

	public void silentlyClose() {
		return;
	}
}
