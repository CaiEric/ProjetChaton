package fr.upem.chaton.server;

import java.nio.channels.SelectionKey;
import java.util.Objects;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.ProcessStatus;
import fr.upem.chaton.protocole.TypeRequest;
import fr.upem.chaton.reader.DataReader;
import fr.upem.chaton.server.database.DataBase;
import fr.upem.chaton.writer.DataWriter;

public abstract class ServerContextAbstract implements ServerContextInterface {

	//private static final Logger logger = Logger.getLogger(ServerContextAbstract.class.getName());
	
	final DataReader reader;
	final DataWriter writer;
	final Server server;
	final DataBase database;
	final SelectionKey clientKey;
	DataChaton data;
	TypeRequest request = TypeRequest.EMPTY;

	public ServerContextAbstract(Server server, DataBase dataBase, SelectionKey clientKey, DataReader reader,
			DataWriter writer) {
		this.server = Objects.requireNonNull(server);
		this.database = Objects.requireNonNull(dataBase);
		this.clientKey = Objects.requireNonNull(clientKey);
		this.reader = Objects.requireNonNull(reader);
		this.writer = Objects.requireNonNull(writer);
	}

	/**
	 * read the data
	 */
	@Override
	public ProcessStatus processIn() {
		var status = reader.process();
		if (status != ProcessStatus.DONE) {
			return status;
		}

		data = (DataChaton) reader.get();

		reader.reset();
		request = data.getType();

		status = processRequest();
		return status;
	}

	/**
	 * write the data
	 */
	@Override
	public ProcessStatus processOut(DataChaton data) {
		Objects.requireNonNull(data);
		return writer.process(data);
	}

	abstract ProcessStatus processRequest();
}
