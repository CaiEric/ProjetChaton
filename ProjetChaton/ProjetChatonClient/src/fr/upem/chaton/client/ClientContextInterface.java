package fr.upem.chaton.client;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.ProcessStatus;

public interface ClientContextInterface {

	void authentification();
	
	ProcessStatus processIn();

	ProcessStatus processOut(DataChaton data);

	void silentlyClose();
}
