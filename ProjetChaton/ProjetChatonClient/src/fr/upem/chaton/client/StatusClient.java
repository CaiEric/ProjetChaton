package fr.upem.chaton.client;

public enum StatusClient {
	CONNECTED, DISCONNECTED, TCP_CONNECTED
}
