package fr.upem.chaton.client;

import java.nio.channels.SelectionKey;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Queue;
import java.util.logging.Logger;

import fr.upem.chaton.ProcessStatus;
import fr.upem.chaton.reader.DataReader;
import fr.upem.chaton.writer.DataWriter;

public class ClientContextPublic extends ClientContextAbstract implements ClientContextInterface{

	private enum ClientStatus {
		WAITING, 
		WAITING_AUTHENTIFICATION_RESPONSE, 
		WAITING_PRIVATE_CONNECTION_REMOTE_RESPONSE, 
		WAITING_PRIVATE_CONNECTION_LOCAL_RESPONSE, 
		WAITING_TOKEN};
	
	private static final Logger logger = Logger.getLogger(ClientContextPublic.class.getName());
	private final List<ClientStatus> clientStatus = new ArrayList<>(Arrays.asList(ClientStatus.WAITING_AUTHENTIFICATION_RESPONSE));
	private final String clientLogin;
	private final Queue<String> foreignsWaitingPrivateConnection = new ArrayDeque<>();
	private String isSender = "false";
	
	public ClientContextPublic(Client client, SelectionKey publicKey, String clientLogin, DataReader reader, DataWriter writer) {
		super(client, publicKey, reader, writer);
		this.clientLogin = clientLogin;
	}
	
	/**
	 * The method is use to send a authentification request to server
	 */
	public void authentification() {
		//logger.info("Try to connect with login");
		client.send(clientKey, ClientMessage.authentificationRequest(clientLogin));
	}
	
	/**
	 * processIn specific to public connection
	 */
	
	ProcessStatus processInRequest() {
		switch(request) {
		case PUBLIC_MESSAGE: return processInPublicMessage();
		case PRIVATE_MESSAGE: return processInPrivateMessage();
		case PRIVATE_CONNECTION : return processInPrivateConnection();
		case ACCEPT: return processInAccept();
		case REFUSE: return processInRefuse();
		case TOKEN_SERVER: return processInToken();
		default : return ProcessStatus.DONE;
		}
	}
	
	/**
	 * processOut specific to public connection
	 */
	
	ProcessStatus processOutRequest() {
		switch(request) {
		case PRIVATE_CONNECTION : return processOutPrivateConnection();
		case PUBLIC_MESSAGE: return processOutPublicMessageOrResponseMessage();
		default : return ProcessStatus.DONE;
		}
	}
	
	// --------------------------------- PROCESS IN --------------------------------------
	
	private ProcessStatus processInToken() {
		// logger.info("Process Token");
		
		if(!clientStatus.contains(ClientStatus.WAITING_TOKEN)) {
			logger.info("The server send you a token but you not waiting that");
			return ProcessStatus.ERROR;
		}
		
		var isSend = isSender;
		isSender = "false";
		return client.createNewPrivateConnection(dataIn, isSend);
	}

	private ProcessStatus processInRefuse() {

		var foreignLogin = dataIn.poll();
		
		if(clientStatus.contains(ClientStatus.WAITING_AUTHENTIFICATION_RESPONSE)) {
			logger.info("Authentification failure to server");
			clientStatus.remove(ClientStatus.WAITING_AUTHENTIFICATION_RESPONSE);
			return ProcessStatus.ERROR;
		}
		
		if(clientStatus.contains(ClientStatus.WAITING_PRIVATE_CONNECTION_LOCAL_RESPONSE)) {
			logger.info("The server prevent you the foreign client is deconnected");
			clientStatus.remove(ClientStatus.WAITING_PRIVATE_CONNECTION_LOCAL_RESPONSE);
			return ProcessStatus.DONE;
		}
		
		logger.info(foreignLogin + " refuse for private connection");
		clientStatus.remove(ClientStatus.WAITING_PRIVATE_CONNECTION_REMOTE_RESPONSE);
		
		return ProcessStatus.DONE;
	}

	private ProcessStatus processInAccept() {
		
		var foreignLogin = dataIn.poll();
		
		if(clientStatus.contains(ClientStatus.WAITING_AUTHENTIFICATION_RESPONSE)) {
			System.out.println("Authentification success to server");
			clientStatus.remove(ClientStatus.WAITING_AUTHENTIFICATION_RESPONSE);
			return ProcessStatus.DONE;
		}
		
		if(!clientStatus.contains(ClientStatus.WAITING_PRIVATE_CONNECTION_REMOTE_RESPONSE)) {
			logger.info("The server send you a incoherent request : " + request);
			return ProcessStatus.ERROR;
		}

		logger.info(foreignLogin + " accept for private connection");
		clientStatus.remove(ClientStatus.WAITING_PRIVATE_CONNECTION_REMOTE_RESPONSE);
		clientStatus.add(ClientStatus.WAITING_TOKEN);
		return ProcessStatus.DONE;
	}

	private ProcessStatus processInPrivateConnection() {
		
		var foreignLogin = dataIn.poll();
		
		// logger.info(foreignLogin + " wish create a private connection with you : Y/N ?");
		System.out.println(foreignLogin + " wish create a private connection with you : Y/N ?");
		clientStatus.add(ClientStatus.WAITING_PRIVATE_CONNECTION_LOCAL_RESPONSE);
		foreignsWaitingPrivateConnection.add(foreignLogin);
		
		return ProcessStatus.DONE;
	}

	private ProcessStatus processInPrivateMessage() {
		
		var foreignLogin = dataIn.poll();
		var msg = dataIn.poll();
		
		System.out.println("[Privé] " + foreignLogin + " : " + msg);
		return ProcessStatus.DONE;
	}

	private ProcessStatus processInPublicMessage() {
		
		var foreignLogin = dataIn.poll();
		var msg = dataIn.poll();
		
		System.out.println("[Public] " + foreignLogin + " : " + msg);
		return ProcessStatus.DONE;
	}

	// --------------------------------- PROCESS OUT --------------------------------------
	
	private ProcessStatus processOutPublicMessageOrResponseMessage() {
		
		// var op = dataOut.get(0);
		//var login = dataOut.get(1);
		var response = dataOut.get(2);
		
		if(!clientStatus.contains(ClientStatus.WAITING_PRIVATE_CONNECTION_LOCAL_RESPONSE)) {
			//logger.info("Sending a public message");
			return ProcessStatus.DONE;
		}
		
		switch(response) {
		case "Y": 
			dataOut = ClientMessage.acceptResponse(foreignsWaitingPrivateConnection.poll()); 
			clientStatus.add(ClientStatus.WAITING_TOKEN);
			break;
		case "N": dataOut = ClientMessage.refuseResponse(foreignsWaitingPrivateConnection.poll()); break;
		default : return ProcessStatus.DONE;
		}
		

		// logger.info("Sending response for a private connection " + login  + " " + response);
		clientStatus.remove(ClientStatus.WAITING_PRIVATE_CONNECTION_LOCAL_RESPONSE);
		return ProcessStatus.DONE;
	}

	private ProcessStatus processOutPrivateConnection() {
		clientStatus.add(ClientStatus.WAITING_PRIVATE_CONNECTION_REMOTE_RESPONSE);
		isSender = "true";
		return ProcessStatus.DONE;
	}
	
	@Override
	public void silentlyClose() {
	}

}
