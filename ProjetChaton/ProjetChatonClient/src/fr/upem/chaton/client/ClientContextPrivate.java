package fr.upem.chaton.client;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.Logger;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.ProcessStatus;
import fr.upem.chaton.protocole.TypeRequest;
import fr.upem.chaton.reader.DataReader;
import fr.upem.chaton.reader.HTTPReader;
import fr.upem.chaton.writer.DataWriter;
import fr.upem.chaton.writer.HTTPWriter;

public class ClientContextPrivate implements ClientContextInterface{

	private enum Status {DISCONNECT, STARTING, CONNECT};
	private static final Logger logger = Logger.getLogger(ClientContextPrivate.class.getName());
	private final String token;
	private final Client client;
	private final SelectionKey clientKey;
	private Status state = Status.DISCONNECT;
	private final DataReader reader;
	private final DataWriter writer;
	private final HTTPReader httpReader ;
	private final HTTPWriter httpWriter;
	private String file;
	private boolean isSender;
	private DataChaton data;
	
	public ClientContextPrivate(Client client, SelectionKey clientKey, ByteBuffer bbin, ByteBuffer bbout, String token, String file, DataReader reader, DataWriter writer, boolean isSender) {
		
		this.client = client;
		this.clientKey = clientKey;
		this.isSender = isSender;
		this.token = token;
		this.reader = reader;
		this.writer = writer;
		this.httpReader = new HTTPReader(bbin);
		this.httpWriter = new HTTPWriter(bbout);
		this.file = file;
	}
	
	/**
	 * The method is use to send a authentification request to server
	 */
	public void authentification() {
		// logger.info("Try to connect with token");
		client.send(clientKey, ClientMessage.authentificationTokenRequest(token));
	}
	
	/**
	 * processIn specific to private connection
	 */
	@Override
	public ProcessStatus processIn() { // Traitement quand recoit
		//logger.info("Process In private Connection");
		
		if(state == Status.DISCONNECT) {
			// logger.info("Process In Authentification");
			var s = reader.process();
			if (s != ProcessStatus.DONE) {
				return s;
			}

			data = (DataChaton) reader.get();

			reader.reset();
			var request = data.getType();
			// logger.info("REQUEST : " + request);
			
			if(request == TypeRequest.ACCEPT) {
				state = Status.CONNECT;
				System.out.println("Authentification success to server");
				//logger.info("Authentification success to server");
				if(isSender == true )
					client.send(clientKey, ClientMessage.httpGetRequest(file));
				return ProcessStatus.DONE;
			}
			
			System.out.println("Authentification failure to server");
			//logger.info("Authentification failure to server");
			return ProcessStatus.ERROR;
		}
		// logger.info("Process In HTTP");
		
		try {
			var status = httpReader.process();
			if(status != ProcessStatus.DONE) {
				if(status == ProcessStatus.ERROR) 
					logger.info(httpReader.getError());
				return status;
			}
			data = (DataChaton) httpReader.get();
		} finally {
			httpReader.reset();
		}
		
		switch(data.getType()) {
		case HTTP_GET : return processInHTTPGet();
		case HTTP_GET_RESPONSE : return processInHTTPGetResponse();
		default : return ProcessStatus.DONE;
		}
	}
	private ProcessStatus processInHTTPGet() {
		// logger.info("processInHTTPGet");
		// logger.info("Line " + data.get(0));
		var line = data.get(0);
		var tokens = line.split(" ");
		var req = tokens[0];
		var file = tokens[1];
		var httpVersion = tokens[2];
		
		if(!req.equals("GET") || !httpVersion.equals("HTTP/1.1")) {
			logger.info("Bad Request");
			return ProcessStatus.ERROR;
		}
		
		Path path = Path.of("./" +file);
		if(Files.notExists(path)) {
			logger.info("The path not exist");
			return ProcessStatus.DONE;
		}
		
		if(Files.isDirectory(path)) {
			logger.info("The path is a directory");
			return ProcessStatus.DONE;
		}
		
		var strbld = new StringBuilder();
		try {	
			for (var l : Files.readAllLines(path)) {
				logger.info("OUT : " + l);
				strbld.append(l);
			}
		} catch(IOException e) {
			logger.info("Ioe when reading the file");
			return ProcessStatus.ERROR;
		}
		System.out.println("SENDING the file : " + file);
		client.send(clientKey, ClientMessage.httpServerGetResponseSucces(strbld.toString()));
		return ProcessStatus.DONE;
	}

	private ProcessStatus processInHTTPGetResponse() {
		// logger.info("processInHTTPGetResponse");
		if(file.length() >= 5 && file.subSequence(file.length() - 4, file.length()).equals(".txt")) {
			System.out.println("Contenu du fichier demandé (" + file + "):");
			System.out.println("--------------------------------------------------");
			System.out.println(data.get(0).toString());
			System.out.println("--------------------------------------------------");
		}else {
			try {
				
				var fileInd = file.lastIndexOf("/");
				var dir = client.directory + "/" + file.substring(fileInd+1);
				System.out.println("Sauvegarde du fichier : " + dir);
				var path = Paths.get(dir);
				 Files.writeString(path, data.get(0));
				 
			}catch (IOException e) {
				logger.info("Ioe when writing the file");
				return ProcessStatus.ERROR;
			}
			
		}
			
		return ProcessStatus.DONE;
	}

	/**
	 * process Out specific to private connection
	 */
	@Override
	public ProcessStatus processOut(DataChaton data) { // Traitement avant d'envoyer
		// logger.info("Process Out private Connection");
		
		if(data.getType() == TypeRequest.HTTP_GET_RESPONSE || data.getType() == TypeRequest.HTTP_GET && state == Status.CONNECT) {
			return httpWriter.process(data);
		}
		return writer.process(data);
	}
	
	@Override
	public void silentlyClose() {
	}

}
