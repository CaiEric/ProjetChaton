package fr.upem.chaton.client;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.Channel;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Logger;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.ProcessStatus;
import fr.upem.chaton.protocole.TypeRequest;
import fr.upem.chaton.reader.DataReader;
import fr.upem.chaton.writer.DataWriter;

public class Client {
	private static Logger logger = Logger.getLogger(Client.class.getName());
	private static final int BUFFER_SIZE = 1024;
	private enum ContextType {PUBLIC, PRIVATE};
	
	static class ClientContext {
		final private Client client;
		final private SocketChannel mainSocketChannel;
		final private SelectionKey key;
		final private SocketChannel sc;
		
		final private ByteBuffer bbin = ByteBuffer.allocate(BUFFER_SIZE);
		final private ByteBuffer bbout = ByteBuffer.allocate(BUFFER_SIZE);
		final private Charset UTF8 = Charset.forName("utf8");
		final private Queue<DataChaton> queue = new LinkedBlockingQueue<>(100);
		final private DataReader reader = new DataReader(bbin, UTF8);
		final private DataWriter writer = new DataWriter(bbout, UTF8);
		
		private ClientContextInterface context;
		private boolean closed = false;
		private String foreignLogin;
		
		private ClientContext(Client client, SelectionKey key) {
			this.client = client;
			this.mainSocketChannel = client.mainSocketChannel;
			this.key = key;
			this.sc = (SocketChannel)key.channel();
		}
		
		/**
		 * Init the context 
		 * @param contextType
		 * @param key
		 * @param vals
		 */
		private void initContext(ContextType contextType, SelectionKey key, String... vals) {
			switch(contextType) {
			case PUBLIC: 
				var login = vals[0];
				System.out.println("Connection with login : " + login);
				context = new ClientContextPublic(client, key, login, reader, writer);
				break;
			case PRIVATE:
				var token = vals[0];
				foreignLogin = vals[1];
				var isSend = vals[2];
				boolean isSender = false;
				if(isSend.equals("true"))
					isSender = true;
				context = new ClientContextPrivate(client, key, bbin, bbout, token, client.file, reader, writer, isSender);
				break;
			}
		}
		
		/** 
		 * Process the in data
		 */
		private void processIn() {
			for (;;) {

				var status = context.processIn(); // A mettre dans les contexts
				switch (status) {
				case DONE:
					break;
				case REFILL:
					return;
				case ERROR:
					closed = true;
					return;
				}
			}
		}
		
		/**
		 * Process the out data
		 */
		private void processOut() {
			while (!queue.isEmpty()) {
				var data = queue.peek();
				var status = context.processOut(data);
				switch (status) {
				case DONE:
					queue.poll();
					break;
				case REFILL:
					return;
				case ERROR:
					closed = true;
					return;
				}
			}
		}
		
		/**
		 * Add the msg in the queue and proccessOut, after that updateTheInterestOp
		 * @param msg
		 */
		private void queueMessage(DataChaton msg) {
			synchronized(reader) {
				queue.add(msg);
				processOut();
				updateInterestOps();
			}
		}
		
		private void doRead() throws IOException {
			if (sc.read(bbin) == -1) {
				closed = true;
			}
			processIn();
			updateInterestOps();
		}

		private void doWrite() throws IOException {
			
			bbout.flip();
			sc.write(bbout);
			bbout.compact();
			updateInterestOps();
		}
		
		private void updateInterestOps() {
			int newOp = 0;
			if (bbin.hasRemaining() && !closed) {
				newOp |= SelectionKey.OP_READ;
			}
			if (bbout.position() != 0) {
				newOp |= SelectionKey.OP_WRITE;
			}
			if (newOp == 0) {
				silentlyClose();
			} else {
				key.interestOps(newOp);
			}
		}

		private void silentlyClose() {
			try {
				sc.close();
				client.removeLogin(foreignLogin);
				if(this.sc == mainSocketChannel) {
					System.out.println("Exit the program");
					Thread.currentThread().interrupt();
				}
			} catch (IOException e) {}
		}
	}
	
	
	private final Selector selector;
	private final SocketChannel mainSocketChannel;
	private final InetSocketAddress serverAddress;
	final SelectionKey mainKey;
	final Path directory;
	private final ClientContext mainContext;
	private DataChaton dataConsole;
	private final String login;
	private final Map<String, ClientContext> privateForeigns = new HashMap<>(); 
	private String file;
	
	public Client(String login, String host, String port, Path directory) throws IOException {
		this.selector = Selector.open();
		this.login = login;
		this.serverAddress = new InetSocketAddress(host, Integer.parseInt(port));
		this.directory = directory;

		this.mainSocketChannel = SocketChannel.open();
		this.mainSocketChannel.configureBlocking(false);
		this.mainKey = mainSocketChannel.register(selector, SelectionKey.OP_CONNECT);
		this.mainContext = new ClientContext(this, mainKey);
		this.mainContext.initContext(ContextType.PUBLIC, mainKey, login);
		this.mainKey.attach(mainContext);
	}
	
	/**
	 * Add a message from the console in the client queue
	 * @param type
	 * @param data
	 */
	public void addQueueMessageFromConsole(TypeRequest type, List<String> data) { 
		switch(type) {
		case PRIVATE_MESSAGE: 
			var foreignLogin = data.get(0);
			var message = data.get(1);
			dataConsole = ClientMessage.privateMessageRequest(foreignLogin, message);
			mainContext.queueMessage(dataConsole);
			break;
		case PRIVATE_CONNECTION: // COMMENT UTILISER FILE
			foreignLogin = data.get(0);
			file = data.get(1);
			if(!privateForeigns.containsKey(foreignLogin)) {
				dataConsole = ClientMessage.privateConnectionRequest(foreignLogin);
				mainContext.queueMessage(dataConsole);
			} else {
				var context = privateForeigns.get(foreignLogin);
				context.queueMessage(ClientMessage.httpGetRequest(file));
			}
			break;
		case PUBLIC_MESSAGE:
			message = data.get(0);
			dataConsole = ClientMessage.publicMessageRequest(this.login, message);
			mainContext.queueMessage(dataConsole);
			break;
		default:break;
		}
		
		selector.wakeup();
	}
	
	/**
	 * Add a message to the key given
	 * @param key
	 * @param data
	 */
	public void send(SelectionKey key, DataChaton data) {
		((ClientContext) key.attachment()).queueMessage(data);
	}
	
	/**
	 * Create a new Private Connection
	 * @param dataToken
	 * @param isSender
	 * @return
	 */
	public ProcessStatus createNewPrivateConnection(DataChaton dataToken, String isSender) {
		var token = dataToken.poll();
		var foreignLogin = dataToken.poll();
		SelectionKey privateKey = null;
		
		try {
			var privateSocketChannel = SocketChannel.open();
				privateSocketChannel.configureBlocking(false);
			    privateKey = privateSocketChannel.register(selector, SelectionKey.OP_CONNECT);
				privateSocketChannel.connect(serverAddress);
			
			var privateContext = new ClientContext(this, privateKey);
				privateContext.initContext(ContextType.PRIVATE, privateKey, token, foreignLogin, isSender, file);
				privateKey.attach(privateContext);
				
			privateForeigns.put(foreignLogin, privateContext);
			System.out.println("New Private Connection is established..");
			return ProcessStatus.DONE;
		} catch (IOException e) {
			return ProcessStatus.ERROR;
		}
		
		// A COMPLETER
	}
	
	private void removeLogin(String foreignLogin) {
		System.out.println("Removing private connection");
		while(privateForeigns.remove(foreignLogin) != null);
		
	}
	
	private void treatKey(SelectionKey key) {
		
		if (key.isValid() && key.isConnectable()) {
			try {
				doConnect(key);
			}catch(IOException e) {
				logger.info("Connection close in local");
				throw new Error();
			}
		}
		try {
			if (key.isValid() && key.isWritable()) {
				((ClientContext) key.attachment()).doWrite();
			}
			if (key.isValid() && key.isReadable()) {
				((ClientContext) key.attachment()).doRead();
			}
		} catch (IOException e) {
			logger.info("Connection closed with server due to IOException");
			silentlyClose(key);
		} 
	}
	
	private boolean doConnect(SelectionKey key) throws IOException {
		SocketChannel socket = (SocketChannel) key.channel();
		if(socket.isConnectionPending() && socket.finishConnect()) {
			var context = (ClientContextInterface) ((ClientContext)key.attachment()).context;
			context.authentification();
			key.interestOps(SelectionKey.OP_READ | SelectionKey.OP_WRITE);
			return true;
		}
		return false;
	}
	
	private void silentlyClose(SelectionKey key) {
		Channel sc = (Channel) key.channel();
		try {
			sc.close();
			System.out.println("Exit the program");
			Thread.currentThread().interrupt();
		} catch (IOException e) {
		}
	}
	
	/** 
	 * The client launcher
	 * @throws IOException
	 */
	public void launch() throws IOException{
		System.out.println("Directory  :" + directory.toAbsolutePath());
		
		mainSocketChannel.connect(serverAddress);
		
		var console = new Thread (() -> new Console().run(this));
		console.setDaemon(true);
		console.start();
		
		
		while (!Thread.interrupted()) {
			selector.select(this::treatKey);
		}
	}
	
	public static void main(String[] args) throws IOException {
		if (args.length != 4) {
			usage();
			return;
		}
		var path = Path.of(args[3]);
		if(Files.notExists(path)) {
			System.out.println("The path is not exist : creation of a directory");
			Files.createDirectory(path);
		}
		else if(!Files.isDirectory(path)) {
			System.out.println("The path is not a directory");
			return;
		}
		
		new Client(args[0], args[1], args[2], path).launch();
	}

	private static void usage() {
		System.out.println("Usage : Client login host port directoryName");
	}


}
