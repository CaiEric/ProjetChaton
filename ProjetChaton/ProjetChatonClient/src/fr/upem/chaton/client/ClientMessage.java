package fr.upem.chaton.client;

import java.util.Calendar;
import java.util.Objects;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.http.HTTPError.HTTPErrorCode;
import fr.upem.chaton.protocole.TypeRequest;

public class ClientMessage {
	/**
	 * httpGet Message
	 * @param file
	 * @return
	 */
	public static DataChaton httpGetRequest(String file) {
		var data = new DataChaton();
		data.setType(TypeRequest.HTTP_GET);
		data.add("GET " + file + " HTTP/1.1 \r\n");
		data.add("\r\n");
		return data;
	}
	
	/**
	 * httpGetResponse Message
	 * @param file
	 * @return
	 */
	
	public static DataChaton httpServerGetResponseSucces(String fileData) {
		var data = new DataChaton();
		data.setType(TypeRequest.HTTP_GET_RESPONSE);
		data.add("HTTP/1.1 200 OK\r\n");
		data.add("Date: " + Calendar.getInstance().getTime() + "\r\n");
		data.add("Server: localhost\r\n");
		data.add("Content-Length: " + fileData.length() + "\r\n");
		data.add("\r\n");
		data.add(fileData);
		return data;
	}
	
	/**
	 * httpGetResponse Message
	 * @param file
	 * @return
	 */
	public static DataChaton httpServerGetResponseFailure(HTTPErrorCode code) {
		var data = new DataChaton();
		
		return data;
	}
	
	/**
	 * authentification login Message
	 * @param file
	 * @return
	 */
	public static DataChaton authentificationRequest(String login) {
		var data = new DataChaton();
		data.setType(TypeRequest.AUTHENTIFICATION);
		data.add(String.valueOf(TypeRequest.AUTHENTIFICATION.ordinal()));
		data.add(login);
		return data;
	}

	/**
	 * authentification token Message
	 * @param file
	 * @return
	 */
	public static DataChaton authentificationTokenRequest(String token) {
		Objects.requireNonNull(token);
		var data = new DataChaton();
		data.setType(TypeRequest.TOKEN_CLIENT);
		data.add(String.valueOf(TypeRequest.TOKEN_CLIENT.ordinal()));
		data.add(token);
		return data;
	}
	
	/**
	 * public Message
	 * @param file
	 * @return
	 */
	public static DataChaton publicMessageRequest(String login, String msg) {
		var data = new DataChaton();
		data.setType(TypeRequest.PUBLIC_MESSAGE);
		data.add(String.valueOf(TypeRequest.PUBLIC_MESSAGE.ordinal()));
		data.add(login);
		data.add(msg);
		return data;
	}

	/**
	 * private connection Message
	 * @param file
	 * @return
	 */
	public static DataChaton privateConnectionRequest(String login) {
		var data = new DataChaton();
		data.setType(TypeRequest.PRIVATE_CONNECTION);
		data.add(String.valueOf(TypeRequest.PRIVATE_CONNECTION.ordinal()));
		data.add(login);
		return data;
	}

	/**
	 * private Message
	 * @param file
	 * @return
	 */
	
	public static DataChaton privateMessageRequest(String dest, String msg) {
		var data = new DataChaton();
		data.setType(TypeRequest.PRIVATE_MESSAGE);
		data.add(String.valueOf(TypeRequest.PRIVATE_MESSAGE.ordinal()));
		data.add(dest);
		data.add(msg);
		return data;
	}

	/**
	 * accept Message
	 * @param file
	 * @return
	 */
	public static DataChaton acceptResponse(String login) {
		Objects.requireNonNull(login);

		var data = new DataChaton();
		data.setType(TypeRequest.ACCEPT);
		data.add(String.valueOf(TypeRequest.ACCEPT.ordinal()));
		data.add(login);

		return data;
	}

	/**
	 * refuse Message
	 * @param file
	 * @return
	 */
	
	public static DataChaton refuseResponse(String message) {
		Objects.requireNonNull(message);

		var data = new DataChaton();
		data.setType(TypeRequest.REFUSE);
		data.add(String.valueOf(TypeRequest.REFUSE.ordinal()));
		data.add(message);

		return data;
	}

}
