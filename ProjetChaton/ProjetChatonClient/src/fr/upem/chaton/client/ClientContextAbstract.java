package fr.upem.chaton.client;

import java.nio.channels.SelectionKey;
import java.util.Objects;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.ProcessStatus;
import fr.upem.chaton.protocole.TypeRequest;
import fr.upem.chaton.reader.DataReader;
import fr.upem.chaton.writer.DataWriter;

public abstract class ClientContextAbstract implements ClientContextInterface {

	final Client client;
	final DataReader reader;
	final DataWriter writer;
	final SelectionKey clientKey;
	DataChaton dataIn, dataOut;
	TypeRequest request = TypeRequest.EMPTY;

	public ClientContextAbstract(Client sc, SelectionKey clientKey, DataReader reader, DataWriter writer) {
		this.client = Objects.requireNonNull(sc);
		this.clientKey = Objects.requireNonNull(clientKey);
		this.reader = Objects.requireNonNull(reader);
		this.writer = Objects.requireNonNull(writer);
	}

	/**
	 * process in, read data 
	 */
	@Override
	public ProcessStatus processIn() {
		var status = reader.process();
		if (status != ProcessStatus.DONE)
			return status;

		dataIn = (DataChaton) reader.get();
		reader.reset();
		request = dataIn.getType();

		return processInRequest();
	}

	/**
	 * processOut, write data
	 */
	@Override
	public ProcessStatus processOut(DataChaton data) {
		Objects.requireNonNull(data);
		
		request = data.getType();
		
		this.dataOut = data;
		var status = processOutRequest();
		if (status != ProcessStatus.DONE)
			return status;
		
		// System.out.println("Get Type after : " + dataOut.getType() + " before " + data.getType());
		
		status = writer.process(dataOut);
		return status;
		
	}

	abstract ProcessStatus processInRequest();
	abstract ProcessStatus processOutRequest();
}
