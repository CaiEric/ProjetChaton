package fr.upem.chaton.client;

import java.util.Arrays;
import java.util.Scanner;
import java.util.stream.Collectors;

import fr.upem.chaton.protocole.TypeRequest;

public class Console {
	
	public void run(Client client) {
		try (Scanner scan = new Scanner(System.in)) {
			while (scan.hasNextLine()) {
				var str = scan.nextLine();
				var tokens = str.split(" ");
				String login;
				
				if(str.equals("")) continue;
				
				switch (tokens[0].charAt(0)) {
					case '@':
						if(tokens.length < 2) {
							System.out.println("Usage PrivateMessage : @login message");
							break;
						}
						
						login = tokens[0].substring(1);
						var message = Arrays.stream(tokens).skip(1).collect(Collectors.joining(" "));
						
						client.addQueueMessageFromConsole(TypeRequest.PRIVATE_MESSAGE, Arrays.asList(login, message));
						break;
					case '/':
						if(tokens.length != 2) {
							System.out.println("Usage PrivateConnection : /login file");
							break;
						}
						login = tokens[0].substring(1);
						var file = tokens[1];
						client.addQueueMessageFromConsole(TypeRequest.PRIVATE_CONNECTION, Arrays.asList(login, file)); 
						break;
					default :
						client.addQueueMessageFromConsole(TypeRequest.PUBLIC_MESSAGE, Arrays.asList(str));
						break;
				}
			}
		}
	}
}
