package fr.upem.chaton.reader;

import fr.upem.chaton.ProcessStatus;

public interface Reader {

	public ProcessStatus process();

	public Object get();

	public void reset();

}
