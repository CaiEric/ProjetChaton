package fr.upem.chaton.reader;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.logging.Logger;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.ProcessStatus;
import fr.upem.chaton.http.HTTPError;
import fr.upem.chaton.http.HTTPError.HTTPErrorCode;
import fr.upem.chaton.http.HTTPException;
import fr.upem.chaton.http.HTTPHeader;
import fr.upem.chaton.protocole.TypeRequest;

public class HTTPReader implements Reader{
	
	private static Logger logger = Logger.getLogger(HTTPReader.class.getName());
	private static final int MAX_SIZE_HEADER_LINE = 8192;
	private static final boolean TREAT_CHUNK_FALSE = false;
	private final Charset ASCII_CHARSET = Charset.forName("ASCII");
	
	private enum State {
		DONE, WAITING_HTTP, WAITING_HEADER, WAITING_CONTENT, ERROR
	};

	private State state = State.WAITING_HTTP;
	private final ByteBuffer bbin;
	private DataChaton data = new DataChaton();
	private HTTPHeader httpHeader;
	private HTTPError httpError = new HTTPError();
    private int headerLength = 0;
	private StringBuilder strBuilder = new StringBuilder();
    HashMap<String, String> fields = new HashMap<>();
    private int line = 0;
    private int contentLengthGiven = 0;
    private int contentLengthCount = 0;
    private boolean cr = false;
    
    public HTTPReader(ByteBuffer bbin) {
        this.bbin = bbin;
    }
    
    /**
     * Read the http Request and return the ProcessStatus corresponding
     */
	@Override
	public ProcessStatus process() {
		
		if (state == State.DONE || state == State.ERROR) {
			throw new IllegalStateException();
		}
		
		var status = readHeader();
		if(status != ProcessStatus.DONE) {
			return status;
		}
		return readContent();
	}

    /**
     * Read the http protocole crlf line and return the ProcessStatus corresponding
     */
	
    private ProcessStatus readLineCRLF() {
    	
    	bbin.flip();
		try {
			while(bbin.hasRemaining()) {
				
				var car = (char) bbin.get();
				
				headerLength++;
				if((state == State.WAITING_HTTP || state == State.WAITING_HEADER) && headerLength >= MAX_SIZE_HEADER_LINE) {
					
					logger.info("The length for the header is too long for a normal header");
					httpError.setError(HTTPErrorCode.EntityTooLarge_413);
					state = State.ERROR;
					return ProcessStatus.ERROR;
				}
				if(state == State.WAITING_CONTENT) {
					contentLengthCount++;
					if(contentLengthCount >= contentLengthGiven) {
						httpError.setError(HTTPErrorCode.EntityTooLarge_413);
						state = State.ERROR;
						return ProcessStatus.ERROR;
					}
				}
				if(cr && car == '\n') {
					cr = false;
					
					var str = strBuilder.substring(0, strBuilder.length() - 1);
					data.add(str);
					this.line++;
					strBuilder = new StringBuilder();
					return ProcessStatus.DONE;
				}
				cr = (car == '\r');
				strBuilder.append(car);
			}
		}finally {
			bbin.compact();
		}
		
		return ProcessStatus.REFILL;
    }
  	
    /**
     * Read the readHeader and return the ProcessStatus corresponding
     * @return A ProcessStatus :
     * 				DONE : finish to read the header
     * 				REFILL : not finish to read the header
     *				ERROR : For retrieves the error need call this.getError();
     */
    
    public ProcessStatus readHeader(){
		if(state == State.WAITING_HTTP) {
			var status = readLineCRLF();
			if(status != ProcessStatus.DONE) {
				return status;
			}
			if(data.get(0).equals("") || data.get(0).length() < 4 /* GET, POST, 404 */) {
				logger.info("Bad request");
				httpError.setError(HTTPErrorCode.BadRequest_400);
				state = State.ERROR;
				return ProcessStatus.ERROR;
			}
			
			state = State.WAITING_HEADER;
			
		}
		
		if(state == State.WAITING_HEADER) {
			var status = readLineCRLF();
			if(status != ProcessStatus.DONE) {
				return status;
			}
			if(data.get(line-1).equals("")) {
				data.remove(line-1);
				line--;
				
				if(data.get(0).substring(0,3).toUpperCase().contains("GET")) {
					// logger.info("Method Get present " + data.get(0));
					state = State.DONE;
					data.setType(TypeRequest.HTTP_GET);
					return ProcessStatus.DONE;
				}
				
				if(data.get(0).substring(0,4).toUpperCase().contains("POST")) {
					logger.info("Forbidden Request");
					httpError.setError(HTTPErrorCode.ForbiddenRequest_403);
					state = State.ERROR;
					return ProcessStatus.ERROR;
				}
				
				for(int i = 1; i < line; i++) {
					var field = data.get(i);
		        	var sep = field.indexOf(":");
		        	var key = field.substring(0, sep);
		        	var value = field.substring(sep+1, field.length());
		        	fields.computeIfPresent(key, (k, oldvalue) -> oldvalue.concat(";").concat(value));
		        	fields.computeIfAbsent(key, o -> value);
			    }
				try {
					httpHeader = HTTPHeader.create(data.get(0), fields);
					contentLengthGiven = httpHeader.getContentLength();
					
					if(contentLengthGiven == -1) {
						logger.info("There are not content-Length");
						httpError.setError(HTTPErrorCode.LengthRequired_411);
						state = State.ERROR;
						return ProcessStatus.ERROR;
					}
					if(httpHeader.isChunkedTransfer() != TREAT_CHUNK_FALSE) {
						logger.info("The server can't treat with chunk transfer");
						httpError.setError(HTTPErrorCode.BadRequest_400);
						state = State.ERROR;
						return ProcessStatus.ERROR;
					}
					
					for(int i = 0; i < line ; i++) {
						data.remove(0);
					}
					line = 0;
				}catch(HTTPException e) {
					logger.info("Something is wrong with the HTTP Header");
					httpError.setError(HTTPErrorCode.BadRequest_400);
					state = State.ERROR;
					return ProcessStatus.ERROR;
				}
				state = State.WAITING_CONTENT;
			}
			return readHeader();
		}
		
		return ProcessStatus.DONE;
    }
    
    /**
     * Read the http content and return the ProcessStatus corresponding
     */
    public ProcessStatus readContent() {
    	if(state == State.WAITING_CONTENT) {
    		bbin.flip();
    		try {
    			var oldSize = bbin.limit();
    			var goneToBeRead = oldSize;
    			var needToBeRead = contentLengthGiven - contentLengthCount;
    			if(goneToBeRead > needToBeRead) {
    				goneToBeRead = needToBeRead;
    			}
    			bbin.limit(goneToBeRead);
    			contentLengthCount += goneToBeRead;
    			strBuilder.append(ASCII_CHARSET.decode(bbin).toString());
    			bbin.limit(oldSize);
    			
    			if(contentLengthCount < contentLengthGiven) {
    				return ProcessStatus.REFILL;
    			}
    			data.add(strBuilder.toString());
    			line++;
    	    	state = State.DONE;
    			data.setType(TypeRequest.HTTP_GET_RESPONSE);
    		}
    		finally {
    			bbin.compact();
    		}
    	}
    	return ProcessStatus.DONE;
    }
    
    /**
     * get the http content in a DataChaton
     */
	public Object get() {
		if (state != State.DONE) {
			throw new IllegalStateException();
		}
		return data;
	}

    /**
     * get the http header
     */
	public HTTPHeader getHeader() {
		if (state != State.DONE) {
			throw new IllegalStateException();
		}
		return httpHeader;
	}
	
    /**
     * get the http error 
     */
	public String getError() {
		if (state != State.ERROR) {
			throw new IllegalStateException();
		}
		return httpError.getError();
	}
	
    /**
     * reset the reader
     */
	public void reset() {
		httpError = new HTTPError();
		strBuilder = new StringBuilder();
	    fields = new HashMap<>();
		headerLength = 0;
	    contentLengthGiven = 0;
	    contentLengthCount = 0;
		line = 0;
		cr = false;
		data = new DataChaton();
		state = State.WAITING_HTTP;
	};
	
}
