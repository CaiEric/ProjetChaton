package fr.upem.chaton.reader;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Objects;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.ProcessStatus;
import fr.upem.chaton.protocole.Protocole;
import fr.upem.chaton.protocole.Protocole.Atom;
import fr.upem.chaton.reader.atomic.AtomicReader;
import fr.upem.chaton.reader.atomic.OpReader;

public class DataReader implements Reader {
	//private static final Logger logger = Logger.getLogger(DataReader.class.getName());

	private enum State {
		DONE, WAITING_OP, WAITING, ERROR
	};

	private final ByteBuffer bb;
	private final Charset charset;
	private final OpReader opReader;
	private final DataReaderBuilder builder = new DataReaderBuilder();

	private State state = State.WAITING_OP;
	private DataChaton data = new DataChaton();
	private List<AtomicReader> atomicReaders;
	private List<Atom> config;
	private int alreadyDo;

	public DataReader(ByteBuffer bb, Charset charset) {
		this.bb = Objects.requireNonNull(bb);
		this.charset = Objects.requireNonNull(charset);
		opReader = new OpReader(bb);
		alreadyDo = 1;
	}

	private ProcessStatus processOp() {
		var status = opReader.process();
		if (status != ProcessStatus.DONE) {
			return status;
		}
		try {
			var op = (Byte) opReader.get();
			var type = Protocole.getTypeRequest((int)op);

			config = Protocole.getConfig(type);
			data.setType(type);
			atomicReaders = builder.setBB(bb).setCharset(charset).setProtocol(config).build();

			//logger.info("Type d'opération lu:" + type);
		} catch (IllegalArgumentException e) { // Not found Op Exception
			return ProcessStatus.ERROR;
		}
		state = State.WAITING;
		return ProcessStatus.DONE;
	}

	/** 
	 * Read in the buffer and put in the DataChaton if it's possible and return the ProcessStatus corresponding
	 * @param data
	 * @return ProcessStatus
	 */
	public ProcessStatus process() {

		if (state == State.DONE || state == State.ERROR) {
			throw new IllegalStateException();
		}

		if (state == State.WAITING_OP) {
			var status = processOp();
			if (status != ProcessStatus.DONE) {
				return status;
			}
		}

		while (alreadyDo < atomicReaders.size()) {

			var atomicReader = atomicReaders.get(alreadyDo);
			var status = atomicReader.process();
			if (status != ProcessStatus.DONE)
				return status;

			var typeAtom = config.get(alreadyDo);
			data.add(DataReaderParser.parse(atomicReader, typeAtom));

			alreadyDo++;
		}

		state = State.DONE;
		return ProcessStatus.DONE;
	}

	/**
	 * get the data
	 */
	public Object get() {
		if (state != State.DONE) {
			throw new IllegalStateException();
		}
		return data;
	}

	/**
	 * reset the reader
	 */
	public void reset() {
		alreadyDo = 1;
		data = new DataChaton();
		opReader.reset();
		state = State.WAITING_OP;
	};
}
