package fr.upem.chaton.reader.atomic;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.chaton.ProcessStatus;

public class IntReader implements AtomicReader {

	private enum State {
		DONE, WAITING, ERROR
	};

	private ByteBuffer bb;
	private State state = State.WAITING;
	private int value;

	public IntReader(ByteBuffer bb) {
		this.bb = Objects.requireNonNull(bb);
	}

	/**
	 * Read a Integer if it's possibleand return the ProcessStatus corresponding
	 * @return ProcessStatus
	 */
	@Override
	public ProcessStatus process() {
		if (state == State.DONE || state == State.ERROR) {
			throw new IllegalStateException();
		}
		bb.flip();
		try {
			if (bb.remaining() >= Integer.BYTES) {
				value = bb.getInt();
				state = State.DONE;
				return ProcessStatus.DONE;
			} else {
				return ProcessStatus.REFILL;
			}
		} finally {
			bb.compact();
		}
	}

	/**
	 * Get a integer 
	 */
	@Override
	public Object get() {
		if (state != State.DONE) {
			throw new IllegalStateException();
		}
		return value;
	}
	
	/**
	 * reset the reader
	 */
	@Override
	public void reset() {
		state = State.WAITING;
	}
}
