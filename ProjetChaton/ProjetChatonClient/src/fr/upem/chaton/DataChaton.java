package fr.upem.chaton;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import fr.upem.chaton.protocole.TypeRequest;

public class DataChaton {
	private TypeRequest type;
	private List<String> data = new ArrayList<>();

	public DataChaton() {
	}

	/**
	 * Add a string 'value' in the arrayList
	 * @param value
	 * @throws UnsupportedOperationException if the {@code add} operation
     *         is not supported by this list
     * @throws ClassCastException if the class of the specified element
     *         prevents it from being added to this list
     * @throws NullPointerException if the specified element is null and this
     *         list does not permit null elements
     * @throws IllegalArgumentException if some property of this element
     *         prevents it from being added to this list
	 */
	public void add(String value) {
		data.add(Objects.requireNonNull(value));
	}

	/**
	 * Retrieve a string with index position in the arraylist
	 * @param index
	 * @return string at index position
	 * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
	 */
	public String get(int index) {
		return data.get(index);
	}

	/**
	 * Retrieve and remove the string with index position in the arraylist
	 * @param index
	 * @return string at index position
	 * @throws UnsupportedOperationException if the {@code remove} operation
     *         is not supported by this list
     * @throws IndexOutOfBoundsException if the index is out of range
     *         ({@code index < 0 || index >= size()})
	 */
	public String remove(int index) {
		return data.remove(index);
	}
	
	/**
	 * Retrieve and remove the string at index 0 in the arraylist
	 * @return string at index 0 or null
	 */
	public String poll() {
		return data.remove(0);
	}

	/**
	 * Set the type of the DataChaton
	 * @param type
	 * @throws NullPointerException if the specified element is null and this
     *     list does not permit null elements
	 */
	public void setType(TypeRequest type) {
		Objects.requireNonNull(type);
		this.type = type;
	}

	/**
	 * Get the type of the DataChaton
	 * @return a TypeRequest
	 */
	public TypeRequest getType() {
		return type;
	}

	/**
	 * Get the size of the arraylist
	 * @return the number of elements in this list
	 */
	public int size() {
		return data.size();
	}

	/**
	 * Reset the arraylist and set the type to null
	 */
	public void reset() {
		data = new ArrayList<>();
		type = null;
	}
}
