package fr.upem.chaton.writer;

import java.util.Objects;

import fr.upem.chaton.ProcessStatus;
import fr.upem.chaton.protocole.Protocole.Atom;
import fr.upem.chaton.writer.atomic.AtomicWriter;
import fr.upem.chaton.writer.atomic.IntWriter;
import fr.upem.chaton.writer.atomic.LongWriter;
import fr.upem.chaton.writer.atomic.OpWriter;
import fr.upem.chaton.writer.atomic.StringWriter;

public interface DataWriterParser {

	/**
	 * Parse the string 'data' and put in the buffer
	 * @param atomicWriter
	 * @param data
	 * @param type
	 * @return
	 */
	public static ProcessStatus parseAndPut(AtomicWriter atomicWriter, String data, Atom type) {
		Objects.requireNonNull(atomicWriter);
		Objects.requireNonNull(data);
		Objects.requireNonNull(type);

		switch (type) {
		case OP:
			return ((OpWriter) atomicWriter).put(Byte.valueOf(data));
		case INT:
			return ((IntWriter) atomicWriter).put(Integer.valueOf(data));
		case LONG:
			return ((LongWriter) atomicWriter).put(Long.valueOf(data));
		case STRING:
			return ((StringWriter) atomicWriter).put(data);
		default:
			throw new IllegalStateException();
		}
	}
}
