package fr.upem.chaton.writer;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;

import fr.upem.chaton.DataChaton;
import fr.upem.chaton.ProcessStatus;

public class HTTPWriter {

	//private static final Logger logger = Logger.getLogger(HTTPWriter.class.getName());
	
	//private enum State {DONE, WAITING, ERROR};
	private ByteBuffer bbout;
	private ByteBuffer dataBB = null;
	private final Charset ASCII = Charset.forName("ASCII");
	
	public HTTPWriter(ByteBuffer bbout) {
		this.bbout = bbout;
	}
	
	/**
	 * Write the data content in the DataChaton in the buffer out and return the ProcessStatus corresponding
	 * @param data
	 * @return ProcessStatus
	 */
	public ProcessStatus process(DataChaton data) {
		
		if(dataBB == null) {
			var strbld = new StringBuilder();
			for(int i = 0; i < data.size(); i++) {
				strbld.append(data.get(i));
			}
			dataBB = ASCII.encode(strbld.toString());
			
			//logger.info("Donnée envoyé : " + strbld);
		}
		
		bbout.put(dataBB);
		dataBB.compact();
		
		if(dataBB.position() != 0) {
			dataBB.flip();
			return ProcessStatus.REFILL;
		}
		reset();
		return ProcessStatus.DONE;
	}
	
	private void reset() {
		dataBB = null;
	}
}
