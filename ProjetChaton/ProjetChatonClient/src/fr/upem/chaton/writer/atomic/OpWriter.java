package fr.upem.chaton.writer.atomic;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.chaton.ProcessStatus;

public class OpWriter implements AtomicWriter {

	private ByteBuffer bb;

	/**
	 * Construct the OpWriter
	 * @param bb
	 */
	
	public OpWriter(ByteBuffer bb) {
		this.bb = Objects.requireNonNull(bb);
	}

	/**
	 * Put a byte in the buffer if it's possible and return the ProcessStatus corresponding
	 * @param data
	 * @return ProcessStatus
	 */
	
	public ProcessStatus put(Byte data) {
		Objects.requireNonNull(data);

		if (bb.remaining() < Byte.BYTES) {
			return ProcessStatus.REFILL;
		}

		bb.put(data.byteValue());
		return ProcessStatus.DONE;
	}
}
