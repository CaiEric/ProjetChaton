package fr.upem.chaton.writer.atomic;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.Objects;

import fr.upem.chaton.ProcessStatus;

public class StringWriter implements AtomicWriter {

	private ByteBuffer bb;
	private Charset charset;

	/**
	 * Construct the StringWriter
	 * @param bb
	 */
	public StringWriter(ByteBuffer bb, Charset charset) {
		this.bb = Objects.requireNonNull(bb);
		this.charset = Objects.requireNonNull(charset);
	}

	/**
	 * Put a String in the buffer if it's possible and return the ProcessStatus corresponding
	 * @param data
	 * @return ProcessStatus
	 */
	public ProcessStatus put(String data) {
		Objects.requireNonNull(data);

		var bbdata = charset.encode(data);
		int size = bbdata.limit();
		if (bb.capacity() < size + Integer.BYTES) {
			return ProcessStatus.ERROR;
		}
		if (bb.remaining() < size + Integer.BYTES) {
			return ProcessStatus.REFILL;
		}

		bb.putInt(size).put(bbdata);
		return ProcessStatus.DONE;
	}
}
