package fr.upem.chaton.writer.atomic;

import java.nio.ByteBuffer;
import java.util.Objects;

import fr.upem.chaton.ProcessStatus;

public class IntWriter implements AtomicWriter {

	private ByteBuffer bb;

	/**
	 * Construct the IntWriter
	 * @param bb
	 */
	public IntWriter(ByteBuffer bb) {
		this.bb = Objects.requireNonNull(bb);
	}

	/**
	 * Put a Integer in the buffer if it's possible and return the ProcessStatus corresponding
	 * @param data
	 * @return ProcessStatus
	 */
	public ProcessStatus put(int data) {
		Objects.requireNonNull(data);

		if (bb.capacity() < Integer.BYTES) {
			throw new IllegalStateException();
		}

		if (bb.remaining() < Integer.BYTES) {
			return ProcessStatus.REFILL;
		}

		bb.putInt(data);
		return ProcessStatus.DONE;
	}
}
