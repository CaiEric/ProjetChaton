package fr.upem.chaton.http;

public class HTTPError{
	
	public enum HTTPErrorCode { BadRequest_400, ForbiddenRequest_403, LengthRequired_411, EntityTooLarge_413 };
	private String errorMessage;
	
	public HTTPError() {
	}
	
	public void setError(HTTPErrorCode code) {
		switch(code) {
		case BadRequest_400:
			errorMessage = "400 Bad Request";
			break;
		case ForbiddenRequest_403:
			errorMessage = "403 Forbidden Request";
			break;
		case LengthRequired_411:
			errorMessage = "411 Length Required";
			break;
		case EntityTooLarge_413: 
			errorMessage = "413 Entity Too Large Error";
			break;
		default :
			throw new IllegalArgumentException();
		}
	}
	
	public String getError() {
		return errorMessage;
	}
}
